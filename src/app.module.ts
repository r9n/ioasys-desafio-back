import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import * as path from 'path'
import { TypeOrmModule } from '@nestjs/typeorm'
import { AuthModule } from './modules/auth/auth.module'
import { AuthService } from './modules/auth/auth.service'
import { MovieModule } from './modules/movie/movie.module'
import { MovieService } from './modules/movie/movie.service'
import { UserModule } from './modules/user/user.module'
import { UserService } from './modules/user/user.service'
import { ConfigModule, ConfigService } from 'nestjs-config'
import { RoleModule } from './modules/auth/roles/role.module'
import { RoleService } from './modules/auth/roles/role.service'
import { PersonController } from './modules/person/person.controller'
import { PersonModule } from './modules/person/person.module'
import { RatingModule } from './modules/rating/rating.module'
import { MovieController } from './modules/movie/movie.controller'
import { UserController } from './modules/user/user.controller'
import { RoleController } from './modules/auth/roles/role.controller'
import { GenresModule } from './modules/movie/genres/genres.module'
import { UserActiveValidation } from './middlewares/user-active-verify.middleware'

@Module({
  imports: [
    ConfigModule.load(path.resolve(__dirname, 'config', '**/!(*.d).{ts,js}')),
    TypeOrmModule.forRootAsync({
      useFactory: (config: ConfigService) => config.get('database'),
      inject: [ConfigService],
    }),
    UserModule,
    AuthModule,
    MovieModule,
    PersonModule,
    RoleModule,
    RatingModule,
    GenresModule,
  ],
  controllers: [
    AppController,
    PersonController,
    MovieController,
    UserController,
    RoleController,
  ],
  providers: [AppService, AuthService, MovieService, UserService, RoleService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): void {
    consumer
      .apply(UserActiveValidation)
      .forRoutes('auth', 'genres', 'movie', 'person', 'user')
  }
}
