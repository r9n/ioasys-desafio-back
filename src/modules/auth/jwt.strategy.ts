import { PassportStrategy } from '@nestjs/passport'
import { Strategy, ExtractJwt } from 'passport-jwt'
import { Injectable, NotFoundException } from '@nestjs/common'
import { JwtPayload } from './jwt-payload.interface'
import { User } from '../user/user.entity'
import { objectDoesNotExistsMessage } from 'src/utils/messages/exceptions.message'
import { UserService } from '../user/user.service'
import { JWT_SECRET_KEY } from 'src/config/jwt'

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private userService: UserService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: JWT_SECRET_KEY,
    })
  }

  async validate(payload: JwtPayload): Promise<User> {
    const { email } = payload
    const user = await this.userService.findUserByEmail(email)
    if (user) {
      return user
    }
    throw new NotFoundException(objectDoesNotExistsMessage('User'))
  }
}
