import { Body, Controller, Post } from '@nestjs/common'
import {
  ApiBody,
  ApiCreatedResponse,
  ApiForbiddenResponse,
} from '@nestjs/swagger'
import { objectAlreadyExistsMessage } from 'src/utils/messages/exceptions.message'
import { signinForbidden } from 'src/utils/messages/forbidden.message'
import { AuthService } from './auth.service'
import { SigninResponseDto } from './dto/signin-response.dto'
import { SigninDto } from './dto/signin.dto'
import { SingupResponseDto } from './dto/signup-response.dto'
import { SignupDto } from './dto/signup.dto'

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('signup')
  @ApiBody({ type: SignupDto })
  @ApiForbiddenResponse({
    description: objectAlreadyExistsMessage('email'),
  })
  @ApiCreatedResponse({ type: SingupResponseDto })
  async signup(@Body() dto: SignupDto): Promise<SingupResponseDto> {
    return this.authService.signup(dto)
  }

  @Post('signin')
  @ApiForbiddenResponse({
    description: signinForbidden,
  })
  @ApiBody({ type: SigninDto })
  @ApiCreatedResponse({ type: SigninResponseDto })
  async singin(@Body() dto: SigninDto): Promise<SigninResponseDto> {
    return this.authService.signin(dto)
  }
}
