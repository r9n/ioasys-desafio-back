import { Module } from '@nestjs/common'
import { UserModule } from '../user/user.module'
import { AuthController } from './auth.controller'
import { AuthService } from './auth.service'
import { RoleModule } from './roles/role.module'
import { JwtModule } from '@nestjs/jwt'
import { JWT_SECRET_KEY, JWT_ESTRATEGIE, tokenExpireTime } from 'src/config/jwt'
import { PassportModule } from '@nestjs/passport'
import { JwtStrategy } from './jwt.strategy'
@Module({
  imports: [
    PassportModule.register({
      defaultStrategy: JWT_ESTRATEGIE,
    }),
    JwtModule.register({
      secret: JWT_SECRET_KEY,
      signOptions: {
        expiresIn: tokenExpireTime,
      },
    }),
    UserModule,
    RoleModule,
  ],
  providers: [AuthService, JwtStrategy],
  controllers: [AuthController],
  exports: [JwtStrategy, PassportModule, JwtModule],
})
export class AuthModule {}
