import {
  ForbiddenException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common'
import { UserService } from '../user/user.service'
import { SigninResponseDto } from './dto/signin-response.dto'
import { SignupDto } from './dto/signup.dto'
import { User } from '../user/user.entity'
import * as bcrypt from 'bcrypt'
import { SigninDto } from './dto/signin.dto'
import {
  objectAlreadyExistsMessage,
  objectDoesNotExistsMessage,
  userAlreadyDesactivated,
} from 'src/utils/messages/exceptions.message'
import { SingupResponseDto } from './dto/signup-response.dto'
import { RoleService } from './roles/role.service'
import { JwtService } from '@nestjs/jwt'
import { JwtPayload } from './jwt-payload.interface'
import { signinForbidden } from 'src/utils/messages/forbidden.message'

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private rolesService: RoleService,
    private jwtService: JwtService,
  ) {}

  async signup(dto: SignupDto): Promise<SingupResponseDto> {
    const newUser: User = new User(dto)
    const hasAccount = await this.userService.findUserByEmail(dto.email)

    if (hasAccount) {
      throw new ForbiddenException(objectAlreadyExistsMessage('e-mail'))
    }

    newUser.salt = await bcrypt.genSalt()

    newUser.role = await this.rolesService.findRoleByName('user')

    newUser.passwordHash = await this.createHasPassword(
      dto.password,
      newUser.salt,
    )

    return this.userService
      .save(newUser)
      .then((user) => {
        return new SingupResponseDto({
          name: user.name,
          lastName: user.lastName,
          email: user.email,
          phoneNumber: user.phoneNumber,
          role: user.role,
        })
      })
      .catch((error) => {
        throw new InternalServerErrorException(error.message)
      })
  }

  async signin(dto: SigninDto): Promise<SigninResponseDto> {
    const user = await this.userService.findUserByEmail(dto.email)

    if (!user) {
      throw new NotFoundException(objectDoesNotExistsMessage('e-mail'))
    }

    if (!user.isActive) {
      throw new NotFoundException(userAlreadyDesactivated)
    }

    const jwtPayload: JwtPayload = { email: dto.email, role: user.role.role }

    const accessToken = this.jwtService.sign(jwtPayload)

    if (this.userService.validateUserPassword(dto)) {
      const signinReponseDto = new SigninResponseDto()
      signinReponseDto.tokenJwt = accessToken
      signinReponseDto.userId = user.id
      return signinReponseDto
    }

    throw new ForbiddenException(signinForbidden)
  }

  private async createHasPassword(
    password: string,
    salt: string,
  ): Promise<string> {
    return bcrypt.hash(password, salt)
  }
}
