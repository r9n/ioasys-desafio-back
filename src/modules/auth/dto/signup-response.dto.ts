import { ApiProperty } from '@nestjs/swagger'
import {
  MAX_USER_NAME_LENGHT,
  MIN_USER_NAME_LENGHT,
  MAX_USER_LAST_NAME_LENGHT,
  MIN_USER_LAST_NAME_LENGHT,
  MAX_PHONE_LENGHT,
  MIN_PHONE_LENGHT,
  MAX_EMAIL_LENGHT,
  MIN_EMAIL_LENGHT,
} from 'src/config/constraints'
import { Role } from '../roles/role.entity'

export class SingupResponseDto {
  @ApiProperty({
    type: String,
    maxLength: MAX_USER_NAME_LENGHT,
    minLength: MIN_USER_NAME_LENGHT,
  })
  name: string

  @ApiProperty({
    type: String,
    maxLength: MAX_USER_LAST_NAME_LENGHT,
    minLength: MIN_USER_LAST_NAME_LENGHT,
  })
  lastName: string

  @ApiProperty({
    type: String,
    maxLength: MAX_PHONE_LENGHT,
    minLength: MIN_PHONE_LENGHT,
  })
  phoneNumber: string

  @ApiProperty({
    type: String,
    maxLength: MAX_EMAIL_LENGHT,
    minLength: MIN_EMAIL_LENGHT,
  })
  email: string

  @ApiProperty({ type: Role })
  role: Role

  constructor(partial: Partial<SingupResponseDto>) {
    Object.assign(this, partial)
  }
}
