import { ApiProperty } from '@nestjs/swagger'
import {
  MAX_USER_NAME_LENGHT,
  MIN_USER_NAME_LENGHT,
  MAX_USER_LAST_NAME_LENGHT,
  MIN_USER_LAST_NAME_LENGHT,
  MAX_PHONE_LENGHT,
  MIN_PHONE_LENGHT,
  MAX_EMAIL_LENGHT,
  MIN_EMAIL_LENGHT,
} from 'src/config/constraints'
import {
  IsString,
  IsNotEmpty,
  IsPhoneNumber,
  Matches,
  MinLength,
  MaxLength,
} from 'class-validator'
export class SignupDto {
  @ApiProperty({
    type: String,
    maxLength: MAX_USER_NAME_LENGHT,
    minLength: MIN_USER_NAME_LENGHT,
    required: true,
  })
  @IsString()
  @IsNotEmpty()
  @MinLength(MIN_USER_NAME_LENGHT)
  @MaxLength(MAX_USER_NAME_LENGHT)
  name: string

  @ApiProperty({
    type: String,
    maxLength: MAX_USER_LAST_NAME_LENGHT,
    minLength: MIN_USER_LAST_NAME_LENGHT,
    required: true,
  })
  @IsString()
  @IsNotEmpty()
  @MinLength(MIN_USER_LAST_NAME_LENGHT)
  @MaxLength(MAX_USER_LAST_NAME_LENGHT)
  lastName: string

  @ApiProperty({
    type: String,
    maxLength: MAX_PHONE_LENGHT,
    minLength: MIN_PHONE_LENGHT,
    required: false,
  })
  @IsString()
  @IsPhoneNumber()
  @MinLength(MIN_PHONE_LENGHT)
  @MaxLength(MAX_PHONE_LENGHT)
  phoneNumber: string

  @ApiProperty({
    type: String,
    maxLength: MAX_EMAIL_LENGHT,
    minLength: MIN_EMAIL_LENGHT,
    required: true,
  })
  @IsString()
  @IsNotEmpty()
  @MinLength(MIN_EMAIL_LENGHT)
  @MaxLength(MAX_EMAIL_LENGHT)
  email: string

  @ApiProperty({
    type: String,
    maxLength: MAX_EMAIL_LENGHT,
    minLength: MIN_EMAIL_LENGHT,
    required: true,
  })
  @IsString()
  @Matches(
    /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?=.*[0-9])(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
    {
      message:
        'Passwords must contain at least: 8 characters in length and maximum 20 characters, 1 uppercase letter, 1 lowercase letter and 1 number',
    },
  )
  password: string
}
