import { ApiProperty } from '@nestjs/swagger'

export class SigninResponseDto {
  @ApiProperty({ type: String, description: 'UUID' })
  userId: string

  @ApiProperty({ type: String })
  tokenJwt: string
}
