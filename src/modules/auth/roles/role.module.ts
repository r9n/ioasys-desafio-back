import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { RoleController } from './role.controller'
import { Role } from './role.entity'
import { RoleService } from './role.service'
import { RolesRepository } from './roles.repository'

@Module({
  imports: [TypeOrmModule.forFeature([Role, RolesRepository])],
  providers: [RoleService],
  controllers: [RoleController],
  exports: [TypeOrmModule, RoleService],
})
export class RoleModule {}
