import { Injectable } from '@nestjs/common'
import { Role } from './role.entity'
import { RolesRepository } from './roles.repository'

@Injectable()
export class RoleService {
  constructor(private rolesRepository: RolesRepository) {}

  async findRoleByName(role: string): Promise<Role> {
    return this.rolesRepository.findOne({ role })
  }
}
