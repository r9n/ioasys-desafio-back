import { ApiProperty } from '@nestjs/swagger'
import {
  MAX_ROLE_NAME_LENGHT,
  MIN_ROLE_NAME_LENGHT,
} from 'src/config/constraints'
import { User } from 'src/modules/user/user.entity'
import { BaseEntity } from 'src/utils/entities/base-entity'
import { Column, Entity, OneToMany } from 'typeorm'

@Entity()
export class Role extends BaseEntity {
  @Column({ type: 'varchar', length: MAX_ROLE_NAME_LENGHT, nullable: false })
  @ApiProperty({
    type: String,
    maxLength: MAX_ROLE_NAME_LENGHT,
    minLength: MIN_ROLE_NAME_LENGHT,
    required: true,
  })
  role: string

  @OneToMany(() => User, (user) => user.role)
  user: Array<User>

  constructor(partial: Partial<Role>) {
    super()
    Object.assign(this, partial)
  }
}
