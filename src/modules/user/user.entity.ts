import {
  MAX_EMAIL_LENGHT,
  MAX_PHONE_LENGHT,
  MAX_USER_LAST_NAME_LENGHT,
  MAX_USER_NAME_LENGHT,
  MIN_EMAIL_LENGHT,
  MIN_PHONE_LENGHT,
  MIN_USER_LAST_NAME_LENGHT,
  MIN_USER_NAME_LENGHT,
} from 'src/config/constraints'
import { BaseEntity } from 'src/utils/entities/base-entity'
import { Column, Entity, ManyToOne, OneToMany } from 'typeorm'
import { Rating } from '../rating/rating.entity'
import { Role } from '../auth/roles/role.entity'
import { ApiProperty } from '@nestjs/swagger'
import * as bcrypt from 'bcrypt'
import { EditUserDto } from './dto/edit-user.dto'
@Entity()
export class User extends BaseEntity {
  @Column({ type: 'varchar', length: MAX_USER_NAME_LENGHT, nullable: false })
  @ApiProperty({
    type: String,
    maxLength: MAX_USER_NAME_LENGHT,
    minLength: MIN_USER_NAME_LENGHT,
    required: true,
  })
  name: string

  @Column({
    type: 'varchar',
    length: MAX_USER_LAST_NAME_LENGHT,
    nullable: false,
  })
  @ApiProperty({
    type: String,
    maxLength: MAX_USER_LAST_NAME_LENGHT,
    minLength: MIN_USER_LAST_NAME_LENGHT,
    required: true,
  })
  lastName: string

  @Column({ type: 'varchar', length: MAX_PHONE_LENGHT, nullable: true })
  @ApiProperty({
    type: String,
    maxLength: MAX_PHONE_LENGHT,
    minLength: MIN_PHONE_LENGHT,
    required: false,
  })
  phoneNumber: string

  @Column({ type: 'varchar', length: MAX_EMAIL_LENGHT, nullable: false })
  @ApiProperty({
    type: String,
    maxLength: MAX_EMAIL_LENGHT,
    minLength: MIN_EMAIL_LENGHT,
    required: true,
  })
  email: string

  @Column({ type: 'boolean', default: true })
  @ApiProperty({ type: Boolean })
  isActive: boolean

  @OneToMany(() => Rating, (ratings) => ratings.user)
  ratings: Array<Rating>

  @ManyToOne(() => Role, (role) => role.user)
  role: Role

  @Column({ select: false })
  salt: string

  @Column({ select: false })
  passwordHash: string

  constructor(partial: Partial<User>) {
    super()
    Object.assign(this, partial)
  }

  async validationPassword(password: string): Promise<boolean> {
    const hash = await bcrypt.hash(password, this.salt)
    return hash === this.passwordHash
  }

  fromDto(dto: EditUserDto): void {
    this.name = dto.name ? dto.name : this.name
    this.lastName = dto.lastName ? dto.lastName : this.lastName
    this.email = dto.email ? dto.email : this.email
    this.phoneNumber = dto.phoneNumber ? dto.phoneNumber : this.phoneNumber
  }
}
