import { ApiProperty } from '@nestjs/swagger'
import { MinLength, MaxLength } from 'class-validator'
import {
  MAX_USER_NAME_LENGHT,
  MIN_USER_NAME_LENGHT,
  MAX_USER_LAST_NAME_LENGHT,
  MIN_USER_LAST_NAME_LENGHT,
  MAX_PHONE_LENGHT,
  MIN_PHONE_LENGHT,
  MAX_EMAIL_LENGHT,
  MIN_EMAIL_LENGHT,
} from 'src/config/constraints'

export class EditUserDto {
  @ApiProperty({
    type: String,
    maxLength: MAX_USER_NAME_LENGHT,
    minLength: MIN_USER_NAME_LENGHT,
    required: true,
  })
  @MinLength(MIN_USER_NAME_LENGHT)
  @MaxLength(MAX_USER_NAME_LENGHT)
  name: string

  @ApiProperty({
    type: String,
    maxLength: MAX_USER_LAST_NAME_LENGHT,
    minLength: MIN_USER_LAST_NAME_LENGHT,
    required: true,
  })
  @MinLength(MIN_USER_LAST_NAME_LENGHT)
  @MaxLength(MAX_USER_LAST_NAME_LENGHT)
  lastName: string

  @ApiProperty({
    type: String,
    maxLength: MAX_PHONE_LENGHT,
    minLength: MIN_PHONE_LENGHT,
    required: false,
  })
  @MinLength(MIN_PHONE_LENGHT)
  @MaxLength(MAX_PHONE_LENGHT)
  phoneNumber: string

  @ApiProperty({
    type: String,
    maxLength: MAX_EMAIL_LENGHT,
    minLength: MIN_EMAIL_LENGHT,
    required: true,
  })
  @MinLength(MIN_EMAIL_LENGHT)
  @MaxLength(MAX_EMAIL_LENGHT)
  email: string

  constructor(partial: Partial<EditUserDto>) {
    Object.assign(this, partial)
  }
}
