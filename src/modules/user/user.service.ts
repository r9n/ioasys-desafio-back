import {
  BadRequestException,
  ForbiddenException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Roles } from 'src/enums/roles.enum'
import {
  objectDoesNotExistsMessage,
  userAlreadyDesactivated,
} from 'src/utils/messages/exceptions.message'
import {
  forbiddenDeactivate,
  editForbidden,
  forbiddenToReactivate,
} from 'src/utils/messages/forbidden.message'
import { SigninDto } from '../auth/dto/signin.dto'
import { EditUserDto } from './dto/edit-user.dto'
import { User } from './user.entity'
import { UserRepository } from './user.repository'

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
  ) {}

  async save(user: User): Promise<User> {
    return this.userRepository.save(user)
  }

  async findUserByEmail(email: string): Promise<User> {
    return this.userRepository.findOne({ email }, { relations: ['role'] })
  }

  async validateUserPassword(dto: SigninDto): Promise<boolean> {
    const { email, password: password } = dto
    const user = await this.userRepository.findOne({ email })

    return await user.validationPassword(password)
  }

  async editUser(
    requestUser: User,
    dto: EditUserDto,
    userId: string,
  ): Promise<User> {
    if (requestUser.id !== userId && requestUser.role.role !== Roles.ADMIN) {
      throw new ForbiddenException(editForbidden)
    }
    const databaseUser = await this.userRepository.findOne({ id: userId })

    if (!databaseUser) {
      throw new NotFoundException(objectDoesNotExistsMessage('user'))
    }

    databaseUser.fromDto(dto)

    return this.userRepository.save(databaseUser).catch((error) => {
      throw new InternalServerErrorException(error.message)
    })
  }

  async deactivateUser(requestUser: User, userId: string): Promise<User> {
    const user = await this.userRepository.findOne({ id: userId })
    if (!user) {
      throw new NotFoundException(objectDoesNotExistsMessage('user'))
    }

    if (requestUser.id !== userId) {
      throw new ForbiddenException(forbiddenDeactivate)
    }

    if (!user.isActive) {
      throw new BadRequestException(userAlreadyDesactivated)
    }

    user.isActive = false

    return this.userRepository.save(user).catch((error) => {
      throw new InternalServerErrorException(error.message)
    })
  }

  async reactivateUser(user: User): Promise<User> {
    if (!user) {
      throw new NotFoundException(objectDoesNotExistsMessage('user'))
    }

    if (user.role.role !== Roles.ADMIN) {
      throw new ForbiddenException(forbiddenToReactivate)
    }

    if (!user.isActive) {
      throw new BadRequestException(userAlreadyDesactivated)
    }

    user.isActive = true

    return this.userRepository.save(user).catch((error) => {
      throw new InternalServerErrorException(error.message)
    })
  }
}
