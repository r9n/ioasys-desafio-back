import { Body, Controller, Param, Patch, Post, UseGuards } from '@nestjs/common'
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiBody,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
} from '@nestjs/swagger'
import {
  forbiddenDeactivate,
  editForbidden,
  forbiddenToReactivate,
} from 'src/utils/messages/forbidden.message'
import { GetUser } from '../auth/decorators/get-users.decorator'
import { EditUserDto } from './dto/edit-user.dto'
import { User } from './user.entity'
import { AuthGuard } from '@nestjs/passport'
import { UserService } from './user.service'
import { SigninResponseDto } from '../auth/dto/signin-response.dto'
import { SigninDto } from '../auth/dto/signin.dto'
import { objectDoesNotExistsMessage } from 'src/utils/messages/exceptions.message'

@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
@Controller('user')
export class UserController {
  constructor(private userSerivce: UserService) {}

  @Patch(':id/edit')
  @ApiForbiddenResponse({ description: editForbidden })
  @ApiNotFoundResponse({ description: objectDoesNotExistsMessage('user') })
  @ApiCreatedResponse({ type: User })
  async editUser(
    @GetUser() user: User,
    @Body() dto: EditUserDto,
    @Param('id') userId: string,
  ): Promise<User> {
    return this.userSerivce.editUser(user, dto, userId)
  }

  @Patch(':id/deactivate')
  @ApiForbiddenResponse({ description: editForbidden })
  @ApiNotFoundResponse({ description: objectDoesNotExistsMessage('user') })
  @ApiBadRequestResponse({ description: forbiddenDeactivate })
  @ApiCreatedResponse({ type: User })
  async deactivateUser(
    @GetUser() user: User,
    @Param('id') userId: string,
  ): Promise<User> {
    return this.userSerivce.deactivateUser(user, userId)
  }

  @Post(':id/reactivate')
  @ApiForbiddenResponse({
    description: forbiddenToReactivate,
  })
  @ApiNotFoundResponse({ description: objectDoesNotExistsMessage('user') })
  @ApiBody({ type: SigninDto })
  @ApiCreatedResponse({ type: SigninResponseDto })
  async reactivate(@GetUser() user: User): Promise<User> {
    return this.userSerivce.reactivateUser(user)
  }
}
