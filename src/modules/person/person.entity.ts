import { ApiProperty } from '@nestjs/swagger'
import { MAX_PERSON_NAME_LENGHT } from 'src/config/constraints'
import { PersonTypes } from 'src/enums/person-types.enum'
import { BaseEntity } from 'src/utils/entities/base-entity'
import { Column, Entity, ManyToMany } from 'typeorm'
import { Movie } from '../movie/movie.entity'
import { RegisterPersonDto } from './dto/person-register.dto'

@Entity()
export class Person extends BaseEntity {
  @Column({ type: 'varchar', length: MAX_PERSON_NAME_LENGHT, nullable: false })
  @ApiProperty({
    type: String,
    maxLength: MAX_PERSON_NAME_LENGHT,
    required: true,
  })
  name: string

  @Column({ type: 'enum', enum: PersonTypes })
  @ApiProperty({ type: PersonTypes, required: true, nullable: false })
  type: PersonTypes

  @ManyToMany(() => Movie, (movie) => movie.participants)
  movies: Array<Movie>

  constructor(partial: Partial<Person>) {
    super()
    Object.assign(this, partial)
  }
  fromoDto(dto: RegisterPersonDto): void {
    this.name = dto.name
    this.type = dto.type
    this.movies = dto.movies
  }
}
