import {
  BadRequestException,
  ForbiddenException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common'
import { Roles } from 'src/enums/roles.enum'
import { objectAlreadyExistsMessage } from 'src/utils/messages/exceptions.message'
import { forbiddenRegister } from 'src/utils/messages/forbidden.message'
import { User } from '../user/user.entity'
import { RegisterPersonDto } from './dto/person-register.dto'
import { Person } from './person.entity'
import { PersonRepository } from './person.repository'

@Injectable()
export class PersonService {
  constructor(private personRepository: PersonRepository) {}

  async register(user: User, dto: RegisterPersonDto): Promise<Person> {
    const databasePerson = await this.personRepository.findOne({
      name: dto.name,
      type: dto.type,
    })

    if (databasePerson) {
      throw new BadRequestException(
        objectAlreadyExistsMessage(dto.type.toString()),
      )
    }

    if (user.role.role !== Roles.ADMIN) {
      throw new ForbiddenException(forbiddenRegister('new persons'))
    }

    const newPerson = new Person({})
    newPerson.fromoDto(dto)

    return this.personRepository.save(newPerson).catch((error) => {
      throw new InternalServerErrorException(error.message)
    })
  }
}
