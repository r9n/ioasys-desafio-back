import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { PersonController } from './person.controller'
import { Person } from './person.entity'
import { PersonRepository } from './person.repository'
import { PersonService } from './person.service'

@Module({
  imports: [TypeOrmModule.forFeature([Person, PersonRepository])],
  providers: [PersonService],
  controllers: [PersonController],
  exports: [TypeOrmModule, PersonService],
})
export class PersonModule {}
