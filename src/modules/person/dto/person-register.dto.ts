import { ApiProperty } from '@nestjs/swagger'
import {
  IsString,
  IsNotEmpty,
  IsEnum,
  MaxLength,
  MinLength,
} from 'class-validator'
import {
  MAX_PERSON_NAME_LENGHT,
  MIN_PERSON_NAME_LENGHT,
} from 'src/config/constraints'
import { PersonTypes } from 'src/enums/person-types.enum'
import { Movie } from 'src/modules/movie/movie.entity'

export class RegisterPersonDto {
  @ApiProperty({
    type: String,
    maxLength: MAX_PERSON_NAME_LENGHT,
    minLength: MIN_PERSON_NAME_LENGHT,
    required: true,
  })
  @IsString()
  @IsNotEmpty()
  @MinLength(MIN_PERSON_NAME_LENGHT)
  @MaxLength(MAX_PERSON_NAME_LENGHT)
  name: string

  @ApiProperty({ type: PersonTypes, enum: PersonTypes, required: true })
  @IsNotEmpty()
  @IsEnum(PersonTypes)
  type: PersonTypes

  @ApiProperty({ type: [Movie], required: false })
  movies: Array<Movie>
}
