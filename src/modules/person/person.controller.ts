import { Body, Controller, Post, UseGuards } from '@nestjs/common'
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiForbiddenResponse,
} from '@nestjs/swagger'
import { objectAlreadyExistsMessage } from 'src/utils/messages/exceptions.message'
import { forbiddenRegister } from 'src/utils/messages/forbidden.message'
import { GetUser } from '../auth/decorators/get-users.decorator'
import { User } from '../user/user.entity'
import { RegisterPersonDto } from './dto/person-register.dto'
import { Person } from './person.entity'
import { PersonService } from './person.service'
import { AuthGuard } from '@nestjs/passport'

@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
@Controller('person')
export class PersonController {
  constructor(private personService: PersonService) {}

  @Post()
  @ApiBadRequestResponse({ description: objectAlreadyExistsMessage('person') })
  @ApiForbiddenResponse({ description: forbiddenRegister('new persons') })
  async register(
    @GetUser() user: User,
    @Body() dto: RegisterPersonDto,
  ): Promise<Person> {
    return this.personService.register(user, dto)
  }
}
