import { Body, Controller, Post, UseGuards } from '@nestjs/common'
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiForbiddenResponse,
} from '@nestjs/swagger'
import { GetUser } from 'src/modules/auth/decorators/get-users.decorator'
import { User } from 'src/modules/user/user.entity'
import { objectAlreadyExistsMessage } from 'src/utils/messages/exceptions.message'
import { forbiddenRegister } from 'src/utils/messages/forbidden.message'
import { RegisterGenreDto } from './dto/register-genre.dto'
import { Genre } from './genres.entity'
import { GenresService } from './genres.service'
import { AuthGuard } from '@nestjs/passport'

@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
@Controller('genres')
export class GenresController {
  constructor(private genreService: GenresService) {}

  @Post()
  @ApiBadRequestResponse({ description: objectAlreadyExistsMessage('genre') })
  @ApiForbiddenResponse({ description: forbiddenRegister('new genres') })
  async register(
    @GetUser() user: User,
    @Body() dto: RegisterGenreDto,
  ): Promise<Genre> {
    return this.genreService.registerGenre(user, dto)
  }
}
