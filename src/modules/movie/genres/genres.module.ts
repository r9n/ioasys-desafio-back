import { Module } from '@nestjs/common'
import { GenresService } from './genres.service'
import { GenresController } from './genres.controller'
import { TypeOrmModule } from '@nestjs/typeorm'
import { Genre } from './genres.entity'
import { GenreRepository } from './genres.repository'

@Module({
  imports: [TypeOrmModule.forFeature([Genre, GenreRepository])],
  providers: [GenresService],
  controllers: [GenresController],
  exports: [TypeOrmModule, GenresService],
})
export class GenresModule {}
