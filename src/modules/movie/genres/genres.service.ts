import {
  BadRequestException,
  ForbiddenException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common'
import { Roles } from 'src/enums/roles.enum'
import { User } from 'src/modules/user/user.entity'
import {
  objectAlreadyExistsMessage,
  objectDoesNotExistsMessage,
} from 'src/utils/messages/exceptions.message'
import { forbiddenRegister } from 'src/utils/messages/forbidden.message'
import { RegisterGenreDto } from './dto/register-genre.dto'
import { Genre } from './genres.entity'
import { GenreRepository } from './genres.repository'

@Injectable()
export class GenresService {
  constructor(private genreRepository: GenreRepository) {}

  async registerGenre(user: User, dto: RegisterGenreDto): Promise<Genre> {
    const dataBaseGenre = await this.genreRepository.findOne({ name: dto.name })

    if (dataBaseGenre) {
      throw new BadRequestException(objectAlreadyExistsMessage('genre'))
    }

    if (user.role.role !== Roles.ADMIN) {
      throw new ForbiddenException(forbiddenRegister('new genres'))
    }

    const newGenre = new Genre({ name: dto.name })

    return this.genreRepository.save(newGenre).catch((error) => {
      throw new InternalServerErrorException(error.message)
    })
  }

  async getGenreByName(name: string): Promise<Genre> {
    const genre = await this.genreRepository.findOne({ name })
    if (!genre) {
      throw new NotFoundException(objectDoesNotExistsMessage('genre'))
    }
    return genre
  }
}
