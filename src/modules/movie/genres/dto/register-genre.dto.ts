import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty } from 'class-validator'
import {
  MIN_GENRE_NAME_LENGHT,
  MAX_GENRE_NAME_LENGHT,
} from 'src/config/constraints'

export class RegisterGenreDto {
  @ApiProperty({
    type: String,
    minLength: MIN_GENRE_NAME_LENGHT,
    maxLength: MAX_GENRE_NAME_LENGHT,
    required: true,
  })
  @IsNotEmpty()
  name: string
}
