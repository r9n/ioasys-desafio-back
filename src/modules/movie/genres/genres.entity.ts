import { ApiProperty } from '@nestjs/swagger'
import {
  MAX_GENRE_NAME_LENGHT,
  MIN_GENRE_NAME_LENGHT,
} from 'src/config/constraints'
import { BaseEntity } from 'src/utils/entities/base-entity'
import { Column, Entity, OneToMany } from 'typeorm'
import { Movie } from '../movie.entity'

@Entity()
export class Genre extends BaseEntity {
  @Column({ type: 'varchar', length: MAX_GENRE_NAME_LENGHT, nullable: false })
  @ApiProperty({
    type: String,
    maxLength: MAX_GENRE_NAME_LENGHT,
    minLength: MIN_GENRE_NAME_LENGHT,
    required: true,
  })
  name: string

  @OneToMany(() => Movie, (movie) => movie.genre)
  movie: Array<Movie>

  constructor(partial: Partial<Genre>) {
    super()
    Object.assign(this, partial)
  }
}
