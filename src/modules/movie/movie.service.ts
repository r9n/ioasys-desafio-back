import {
  BadRequestException,
  ForbiddenException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common'
import { maxItemsPerPage } from 'src/config/constraints'
import { PostgresErroCodes } from 'src/enums/postgres-erros.enum'
import { Roles } from 'src/enums/roles.enum'
import {
  movieAlreadyRegistered,
  objectDoesNotExistsMessage,
  participantNotFound,
} from 'src/utils/messages/exceptions.message'
import {
  forbiddenRate,
  forbiddenRegister,
} from 'src/utils/messages/forbidden.message'
import { Rating } from '../rating/rating.entity'
import { RatingService } from '../rating/rating.service'
import { User } from '../user/user.entity'
import { FilterMovieDto } from './dto/filter-movie.dto'
import { GetOneMovieDto } from './dto/get-one.dto'
import { PaginatedGetMovieDto } from './dto/paginated-list.dto'
import { RegisterMovieDto } from './dto/register-movie.dto'
import { VoteMovieDto } from './dto/vote-movie.dto'
import { GenresService } from './genres/genres.service'
import { Movie } from './movie.entity'
import { MovieRepositoy } from './movie.repository'

@Injectable()
export class MovieService {
  constructor(
    private movieRepository: MovieRepositoy,
    private ratingService: RatingService,
    private geneService: GenresService,
  ) {}

  async registerMovie(user: User, dto: RegisterMovieDto): Promise<Movie> {
    if (user.role.role !== Roles.ADMIN) {
      throw new ForbiddenException(forbiddenRegister)
    }

    const isAlreadyRegistered = await this.movieRepository.findOne({
      title: dto.title,
      description: dto.description,
    })
    if (isAlreadyRegistered) {
      throw new BadRequestException(movieAlreadyRegistered)
    }

    const genre = await this.geneService.getGenreByName(dto.genre)

    const newMovie = new Movie({})
    newMovie.fromDto(dto)
    newMovie.genre = genre

    return this.movieRepository.save(newMovie).catch((error) => {
      switch (error.code) {
        case PostgresErroCodes.NOTFOUND: {
          throw new NotFoundException(participantNotFound)
        }
        default: {
          throw new InternalServerErrorException(error.message)
        }
      }
    })
  }

  async rate(user: User, dto: VoteMovieDto, movieId: string): Promise<Rating> {
    if (user.role.role !== Roles.USER) {
      throw new ForbiddenException(forbiddenRate)
    }
    const movie = await this.movieRepository.findOne(movieId)

    if (!movie) {
      throw new NotFoundException(objectDoesNotExistsMessage('movie'))
    }

    const rating = new Rating({
      rating: dto.grade,
      ratingDetails: dto.ratingDetails,
      user,
      movie,
    })

    return this.ratingService.save(rating).catch((error) => {
      throw new InternalServerErrorException(error.message)
    })
  }

  async getAll(
    filterDto: FilterMovieDto,
    page: number,
  ): Promise<PaginatedGetMovieDto> {
    const dto = new PaginatedGetMovieDto()
    if (page < 0) {
      page = 0
    }

    const offset = (page - 1) * maxItemsPerPage
    const movies = await this.movieRepository.getAllMovesByFilter(
      filterDto,
      offset,
    )
    dto.page = page
    dto.total = movies.length
    dto.pageCount = movies.length
    dto.data = movies

    return dto
  }

  async getOneMovie(movieId: string): Promise<GetOneMovieDto> {
    return this.movieRepository.getOneMovie(movieId)
  }
}
