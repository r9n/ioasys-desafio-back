import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty } from 'class-validator'
import { Person } from 'src/modules/person/person.entity'
import { Rating } from 'src/modules/rating/rating.entity'

export class RegisterMovieDto {
  @ApiProperty({ type: String, required: true })
  @IsNotEmpty()
  title: string

  @ApiProperty({ type: String, required: true })
  @IsNotEmpty()
  description: string

  @ApiProperty({ type: String, required: true })
  @IsNotEmpty()
  genre: string

  @ApiProperty({ required: false })
  ratings: Array<Rating>

  @ApiProperty({ required: true })
  @IsNotEmpty()
  participants: Array<Person>
}
