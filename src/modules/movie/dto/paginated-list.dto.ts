import { Movie } from '../movie.entity'
export class PaginatedGetMovieDto {
  count: number

  total: number

  page: number

  pageCount: number

  data: Array<Movie>
}
