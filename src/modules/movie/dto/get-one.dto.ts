import { ApiProperty } from '@nestjs/swagger'
import {
  MAX_MOVIE_NAME_LENGHT,
  MIN_MOVIE_NAME_LENGHT,
  MAX_MOVIE_DESCRIPTION_LENGHT,
  MIN_MOVIE_DESCRIPTION_LENGHT,
} from 'src/config/constraints'
import { Person } from 'src/modules/person/person.entity'
import { Rating } from 'src/modules/rating/rating.entity'
import { Genre } from '../genres/genres.entity'

export class GetOneMovieDto {
  @ApiProperty({ type: 'UUID' })
  id: string

  @ApiProperty({
    type: String,
    maxLength: MAX_MOVIE_NAME_LENGHT,
    minLength: MIN_MOVIE_NAME_LENGHT,
  })
  title: string

  @ApiProperty({
    type: String,
    maxLength: MAX_MOVIE_DESCRIPTION_LENGHT,
    minLength: MIN_MOVIE_DESCRIPTION_LENGHT,
  })
  description: string

  @ApiProperty()
  participants: Array<Person>

  @ApiProperty()
  ratings: Array<Rating>

  @ApiProperty({ type: Genre })
  genre: Genre

  @ApiProperty({ type: Number })
  averageRating: number

  constructor(partial: Partial<GetOneMovieDto>) {
    Object.assign(this, partial)
  }
}
