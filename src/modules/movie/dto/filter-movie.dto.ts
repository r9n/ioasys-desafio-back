import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'
import { MovieFilters } from 'src/enums/movie-filters.enum'

export class FilterMovieDto {
  @ApiProperty({ type: String, required: true })
  @IsNotEmpty()
  @IsString()
  filterType: string

  @ApiProperty({
    type: String,
    enum: MovieFilters,
    required: true,
    description: `Pode ser o nome do atore,
    nome do diretore, o gêreno ou nome do filme`,
  })
  @IsNotEmpty()
  filter: MovieFilters
}
