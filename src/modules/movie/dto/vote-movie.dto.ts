import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, Max, Min } from 'class-validator'
import {
  MAX_RATING,
  MAX_RATING_DETAILS_LENGHT,
  MIN_RATING,
  MIN_RATING_DETAILS_LENGHT,
} from 'src/config/constraints'

export class VoteMovieDto {
  @ApiProperty({ type: Number, minimum: MIN_RATING, maximum: MAX_RATING })
  @IsNotEmpty()
  @Min(MIN_RATING)
  @Max(MAX_RATING)
  grade: number

  @ApiProperty({
    type: String,
    maxLength: MAX_RATING_DETAILS_LENGHT,
    minLength: MIN_RATING_DETAILS_LENGHT,
  })
  ratingDetails: string
}
