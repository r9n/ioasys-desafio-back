import { ApiProperty } from '@nestjs/swagger'
import {
  MAX_MOVIE_DESCRIPTION_LENGHT,
  MAX_MOVIE_NAME_LENGHT,
  MIN_MOVIE_DESCRIPTION_LENGHT,
  MIN_MOVIE_NAME_LENGHT,
} from 'src/config/constraints'
import { BaseEntity } from 'src/utils/entities/base-entity'
import {
  Column,
  Entity,
  JoinColumn,
  ManyToMany,
  ManyToOne,
  OneToMany,
} from 'typeorm'
import { Person } from '../person/person.entity'
import { Rating } from '../rating/rating.entity'
import { RegisterMovieDto } from './dto/register-movie.dto'
import { Genre } from './genres/genres.entity'

@Entity('movie')
export class Movie extends BaseEntity {
  @Column({ type: 'varchar', length: MAX_MOVIE_NAME_LENGHT, nullable: false })
  @ApiProperty({
    type: String,
    maxLength: MAX_MOVIE_NAME_LENGHT,
    minLength: MIN_MOVIE_NAME_LENGHT,
    required: true,
  })
  title: string

  @Column({
    type: 'varchar',
    length: MAX_MOVIE_DESCRIPTION_LENGHT,
    nullable: false,
  })
  @ApiProperty({
    type: String,
    maxLength: MAX_MOVIE_DESCRIPTION_LENGHT,
    minLength: MIN_MOVIE_DESCRIPTION_LENGHT,
    required: false,
  })
  description: string

  @OneToMany(() => Rating, (ratings) => ratings.movie)
  ratings: Array<Rating>

  @ManyToMany(() => Person, (person) => person.movies)
  @JoinColumn()
  participants: Array<Person>

  @ManyToOne(() => Genre, (genre) => genre.movie)
  genre: Genre

  constructor(partial: Partial<Movie>) {
    super()
    Object.assign(this, partial)
  }

  fromDto(dto: RegisterMovieDto): void {
    this.title = dto.title
    this.description = dto.description
    this.ratings = dto.ratings
    this.participants = dto.participants
  }
}
