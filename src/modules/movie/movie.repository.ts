import { MovieFilters } from 'src/enums/movie-filters.enum'
import { EntityRepository, Repository } from 'typeorm'
import { FilterMovieDto } from './dto/filter-movie.dto'
import { Movie } from './movie.entity'
import { NotFoundException } from '@nestjs/common'
import { Person } from '../person/person.entity'
import { maxItemsPerPage } from 'src/config/constraints'
import { objectDoesNotExistsMessage } from 'src/utils/messages/exceptions.message'
import { GetOneMovieDto } from './dto/get-one.dto'
@EntityRepository(Movie)
export class MovieRepositoy extends Repository<Movie> {
  async getAllMovesByFilter(
    filterDto: FilterMovieDto,
    offset: number,
  ): Promise<Array<Movie>> {
    switch (filterDto.filterType) {
      case MovieFilters.ACTOR: {
        return this.query(`select * from movie m 
         inner join "person_movies_movie" as "p" on "p"."movieId"  = m.id
         inner join person pe on pe.id  = "p"."personId" 
         where lower(pe.name) LIKE '%${filterDto.filter.toLowerCase()}%' and pe.type = 'actor'
         LIMIT ${maxItemsPerPage} OFFSET ${offset};
         `)
      }
      case MovieFilters.DIRECTOR: {
        return this.query(`select * from movie m 
          inner join "person_movies_movie" as "p" on "p"."movieId"  = m.id
          inner join person pe on pe.id  = "p"."personId" 
          where lower(pe.name) LIKE '%${filterDto.filter.toLowerCase()}%' and pe.type = 'director'
          LIMIT ${maxItemsPerPage} OFFSET ${offset};
          `)
      }
      case MovieFilters.GENRE: {
        return this.query(`select * from movie m 
          inner join "genre" as "g" on "g"."movieId"  = m.id
          where lower(g.name) LIKE '%${filterDto.filter.toLowerCase()}%'
          LIMIT ${maxItemsPerPage} OFFSET ${offset};
          `)
      }
      case MovieFilters.TITLE: {
        return this.query(`select * from movie m 
          where lower(m.title) LIKE '%${filterDto.filter.toLowerCase()}%'
          LIMIT ${maxItemsPerPage} OFFSET ${offset};
          `)
      }
      default: {
        return this.query(`select * from movie m 
          where lower(m.title) LIKE '%${filterDto.filter.toLowerCase()}%'
          LIMIT ${maxItemsPerPage} OFFSET ${offset};
          `)
      }
    }
  }

  async getOneMovie(movieId: string): Promise<GetOneMovieDto> {
    const movie = await this.query(
      `select distinct m.id,m.title,m.description,
     pe.name as person_name,pe.type,pe.id as person_id,
     g.name as genre,
     rating."ratingDetails",rating.rating
    from movie m
    inner join genre g  on m."genreId" = g.id
    inner join "person_movies_movie" as "p" on "p"."movieId"  = m.id
    inner join person pe on pe.id  = "p"."personId"
    inner join rating rating on  rating."movieId" = m.id
    where m.id = '${movieId}'
    `,
    )

    const movieDto: GetOneMovieDto = this.plainToMoveDto(movie)

    if (!movie) {
      throw new NotFoundException(objectDoesNotExistsMessage('movie'))
    }

    const averageRating = await this.query(`
    select AVG(r.rating) as media from rating r 
    where r."movieId" = '${movieId}'`)

    movieDto.averageRating = this.plainAverageToNumber(averageRating)
    return movieDto
  }

  private plainToMoveDto(result: Array<any>): GetOneMovieDto {
    const moviesInResult = []
    const dtos: Array<GetOneMovieDto> = []
    const movieParticipants: Array<string> = []
    result.forEach((record) => {
      if (!moviesInResult.includes(record.id)) {
        moviesInResult.push(record.id)
        const newDto = new GetOneMovieDto({})
        newDto.id = record.id
        newDto.description = record.description
        newDto.genre = record.genre
        newDto.title = record.title
        newDto.participants = []
        newDto.participants.push(
          new Person({
            id: record.person_id,
            name: record.person_name,
            type: record.type,
          }),
        )
        movieParticipants.push(record.person_id)
        dtos.push(newDto)
      } else {
        dtos.forEach((dto) => {
          if (dto.id === record.id) {
            if (!movieParticipants.includes(record.person_id)) {
              dto.participants.push(
                new Person({
                  id: record.person_id,
                  name: 'person_name',
                  type: record.type,
                }),
              )
              movieParticipants.push(record.person_id)
            }
          }
        })
      }
    })
    return dtos.pop()
  }

  plainAverageToNumber(result: any): number {
    const mediaKeyIndex = 0
    const numberString = result[mediaKeyIndex].media
    return Number(numberString)
  }
}
