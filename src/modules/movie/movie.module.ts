import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { RatingModule } from '../rating/rating.module'
import { GenresModule } from './genres/genres.module'
import { MovieController } from './movie.controller'
import { Movie } from './movie.entity'
import { MovieRepositoy } from './movie.repository'
import { MovieService } from './movie.service'

@Module({
  imports: [
    TypeOrmModule.forFeature([Movie, MovieRepositoy]),
    RatingModule,
    GenresModule,
  ],
  providers: [MovieService],
  controllers: [MovieController],
  exports: [TypeOrmModule],
})
export class MovieModule {}
