import { Body, Controller, Get, Param, Post, UseGuards } from '@nestjs/common'
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
} from '@nestjs/swagger'
import {
  filterInvalid,
  movieAlreadyRegistered,
  objectDoesNotExistsMessage,
  participantNotFound,
} from 'src/utils/messages/exceptions.message'
import {
  forbiddenRate,
  forbiddenRegister,
} from 'src/utils/messages/forbidden.message'
import { GetUser } from '../auth/decorators/get-users.decorator'
import { User } from '../user/user.entity'
import { RegisterMovieDto } from './dto/register-movie.dto'
import { Movie } from './movie.entity'
import { AuthGuard } from '@nestjs/passport'
import { MovieService } from './movie.service'
import { VoteMovieDto } from './dto/vote-movie.dto'
import { Rating } from '../rating/rating.entity'
import { PaginatedGetMovieDto } from './dto/paginated-list.dto'
import { FilterMovieDto } from './dto/filter-movie.dto'
import { GetOneMovieDto } from './dto/get-one.dto'

@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
@Controller('movie')
export class MovieController {
  constructor(private movieService: MovieService) {}

  @Post()
  @ApiCreatedResponse({ type: Movie })
  @ApiForbiddenResponse({ description: forbiddenRegister('movie') })
  @ApiBadRequestResponse({ description: movieAlreadyRegistered })
  @ApiNotFoundResponse({
    description: `${JSON.stringify(participantNotFound)},
                ${JSON.stringify(objectDoesNotExistsMessage('genre'))}`,
  })
  async registerMovie(
    @GetUser() user: User,
    @Body() dto: RegisterMovieDto,
  ): Promise<Movie> {
    return this.movieService.registerMovie(user, dto)
  }

  @Post(':id/vote')
  @ApiCreatedResponse()
  @ApiForbiddenResponse({ description: forbiddenRate })
  @ApiNotFoundResponse({ description: objectDoesNotExistsMessage('movie') })
  async rateMovie(
    @GetUser() user: User,
    @Body() dto: VoteMovieDto,
    @Param('id') movieId: string,
  ): Promise<Rating> {
    return this.movieService.rate(user, dto, movieId)
  }

  @Post('/getall/:page')
  @ApiOkResponse({ type: PaginatedGetMovieDto })
  @ApiBadRequestResponse({ description: filterInvalid })
  async getAll(
    @Body() dto: FilterMovieDto,
    @Param('page') page: number,
  ): Promise<PaginatedGetMovieDto> {
    return this.movieService.getAll(dto, page)
  }

  @Get(':id')
  @ApiOkResponse({ type: GetOneMovieDto })
  @ApiNotFoundResponse({ description: objectDoesNotExistsMessage('movie') })
  async getOne(@Param('id') movieId: string): Promise<GetOneMovieDto> {
    return this.movieService.getOneMovie(movieId)
  }
}
