import { BaseEntity } from 'src/utils/entities/base-entity'
import { Column, Entity, ManyToOne } from 'typeorm'
import { Min, Max } from 'class-validator'
import {
  MAX_RATING,
  MAX_RATING_DETAILS_LENGHT,
  MIN_RATING,
  MIN_RATING_DETAILS_LENGHT,
} from 'src/config/constraints'
import { User } from '../user/user.entity'
import { Movie } from '../movie/movie.entity'
import { ApiProperty } from '@nestjs/swagger'

@Entity()
export class Rating extends BaseEntity {
  @Column({ type: Number })
  @Min(MIN_RATING)
  @Max(MAX_RATING)
  @ApiProperty({
    type: Number,
    minimum: MIN_RATING,
    maximum: MAX_RATING,
    required: true,
  })
  rating: number

  @Column({
    type: 'varchar',
    length: MAX_RATING_DETAILS_LENGHT,
    nullable: true,
  })
  @ApiProperty({
    type: String,
    maxLength: MAX_RATING_DETAILS_LENGHT,
    minLength: MIN_RATING_DETAILS_LENGHT,
  })
  ratingDetails: string

  @ManyToOne(() => User, (user) => user.ratings)
  user: User

  @ManyToOne(() => Movie, (movie) => movie.ratings)
  movie: Movie

  constructor(partial: Partial<Rating>) {
    super()
    Object.assign(this, partial)
  }
}
