import { Injectable } from '@nestjs/common'
import { Rating } from './rating.entity'
import { RatingRepository } from './rating.repository'

@Injectable()
export class RatingService {
  constructor(private ratingRepository: RatingRepository) {}

  async save(rating: Rating): Promise<Rating> {
    return this.ratingRepository.save(rating)
  }
}
