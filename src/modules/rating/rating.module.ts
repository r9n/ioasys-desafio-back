import { Module } from '@nestjs/common'
import { RatingService } from './rating.service'
import { RatingController } from './rating.controller'
import { TypeOrmModule } from '@nestjs/typeorm'
import { Rating } from './rating.entity'
import { RatingRepository } from './rating.repository'

@Module({
  imports: [TypeOrmModule.forFeature([Rating, RatingRepository])],
  providers: [RatingService],
  controllers: [RatingController],
  exports: [TypeOrmModule, RatingService],
})
export class RatingModule {}
