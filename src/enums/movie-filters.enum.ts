export enum MovieFilters {
  ACTOR = 'actor',
  DIRECTOR = 'director',
  TITLE = 'tile',
  GENRE = 'genre',
}
