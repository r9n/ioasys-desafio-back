export function objectAlreadyExistsMessage(objectName: string): string {
  return ` This ${objectName} is already registered in the system`
}

export function objectDoesNotExistsMessage(objectName: string): string {
  return ` This ${objectName} is not registered in the system`
}

export const userAlreadyDesactivated =
  'This user is no longer active in the system'

export const movieAlreadyRegistered = 'This movie already has been registered'

export const participantNotFound =
  'At least one participant informed  was not found in system database'

export const filterInvalid = 'Invalid filter!'

export const weakPassword = `Weak password. Passwords MUST contain at least:
 1 upper case letter
 1 lower case letter
 1 number or special character`
