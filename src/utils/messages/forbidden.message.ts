export const signinForbidden = 'E-mail or password invalid!'

export const editForbidden = "You can't edit an profile of another user"

export const forbiddenDeactivate =
  "You can't deactivate a profile of another user"

export function forbiddenRegister(object: string): string {
  return `Only admins can register ${object}`
}

export const forbiddenRate = 'Only common users can rate movies'

export const forbiddenToReactivate =
  'Only administrators can reactivate an user'
