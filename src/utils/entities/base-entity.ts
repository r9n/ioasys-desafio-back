import { CreateDateColumn, PrimaryGeneratedColumn } from 'typeorm'

export abstract class BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @CreateDateColumn({ select: false })
  createDate: Date

  @CreateDateColumn({ select: false })
  updateDate: Date

  @CreateDateColumn({ select: false })
  version: number
}
