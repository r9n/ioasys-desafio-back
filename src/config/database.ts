import { ConnectionOptions } from 'typeorm'

function getMigrationDirectory() {
  const directory =
    process.env.NODE_ENV === 'migration' ? 'src' : `${__dirname}`
  return `${directory}/migrations/**/*{.ts,.js}`
}

const connectionOptions: ConnectionOptions = {
  type: 'postgres',
  host: 'ioasys-postgres',
  username: 'postgres',
  password: '1234',
  database: 'postgres',
  entities: [`${__dirname}/../**/*.entity.{ts,js}`],
  synchronize: true, // Em produção não devemos deixar isso habilitado, pois pode incorrer  em perda de dados
  dropSchema: false,
  migrationsRun: false,
  logging: ['warn', 'error'],
  logger: 'file',
  migrations: [getMigrationDirectory()],
  cli: {
    migrationsDir: 'src/migrations',
  },
}

export = connectionOptions
