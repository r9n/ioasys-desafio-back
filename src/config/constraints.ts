// API
export const API_NAME = 'IoasysAPI-IMDB'
export const API_TITTLE = 'IOASYS Swagger Documentation'
export const API_DESCRIPTION =
  ' Uma api para o gerenciamento de usuários e filmes'
export const API_VERSION = '1.0'

// Movie
export const MAX_MOVIE_NAME_LENGHT = 36
export const MIN_MOVIE_NAME_LENGHT = 36

export const MAX_MOVIE_DESCRIPTION_LENGHT = 320
export const MIN_MOVIE_DESCRIPTION_LENGHT = 20

// User
export const MAX_USER_NAME_LENGHT = 16
export const MIN_USER_NAME_LENGHT = 4

export const MAX_USER_LAST_NAME_LENGHT = 20
export const MIN_USER_LAST_NAME_LENGHT = 2

export const MAX_PHONE_LENGHT = 18
export const MIN_PHONE_LENGHT = 8

export const MAX_EMAIL_LENGHT = 40
export const MIN_EMAIL_LENGHT = 7

// Persons
export const MAX_PERSON_NAME_LENGHT = 36
export const MIN_PERSON_NAME_LENGHT = 4

// Role
export const MAX_ROLE_NAME_LENGHT = 16
export const MIN_ROLE_NAME_LENGHT = 3

// Rating
export const MAX_RATING = 4
export const MIN_RATING = 0

export const MAX_RATING_DETAILS_LENGHT = 512
export const MIN_RATING_DETAILS_LENGHT = 0

// Auth
export const passwordRegex =
  '/((?=.*d)|(?=.*W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/'

// Movie genre

export const MAX_GENRE_NAME_LENGHT = 16
export const MIN_GENRE_NAME_LENGHT = 3

// Paginating reponses

export const maxItemsPerPage = 25
