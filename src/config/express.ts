const expressDefaultPort = 3000

export default {
  environment: process.env.NODE_ENV,
  port: process.env.PORT || expressDefaultPort,

  isProduction(): boolean {
    return this.get('express.environment') === 'production'
  },
}
