import { ConnectionOptions } from 'typeorm'

const connectionOptions: ConnectionOptions = {
  type: 'postgres',
  host: 'localhost',
  port: Number(5432),
  username: 'postgres',
  password: '1234',
  database: 'postgres',
  entities: [`${__dirname}/../**/*.entity.{ts,js}`],
  migrationsRun: false,
  synchronize: false,
  dropSchema: false,
  migrations: ['src/migrations/*{.ts,.js}'],
  cli: {
    migrationsDir: 'src/migrations',
  },
}

export = connectionOptions
