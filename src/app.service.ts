import { Injectable } from '@nestjs/common'
import { API_VERSION } from './config/constraints'

@Injectable()
export class AppService {
  getHello(): string {
    return `IOASYS IMDB API-VERSION: ${API_VERSION}`
  }
}
