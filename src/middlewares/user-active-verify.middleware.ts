import { UserService } from 'src/modules/user/user.service'
import { Request } from 'express'
import { JwtService } from '@nestjs/jwt'
import {
  Injectable,
  NestMiddleware,
  UnauthorizedException,
} from '@nestjs/common'
import { userAlreadyDesactivated } from 'src/utils/messages/exceptions.message'

@Injectable()
export class UserActiveValidation implements NestMiddleware {
  redisService
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
  ) {}

  async use(req: Request, res: Response, next: () => void): Promise<void> {
    if (req.headers.authorization) {
      const bearerToken = req.headers.authorization.replace('Bearer ', '')
      const payload: any = this.jwtService.decode(bearerToken)
      const user = await this.userService.findUserByEmail(payload.email)
      if (!user.isActive) {
        this.endRequest()
      }
    }
    return next()
  }

  endRequest(): void {
    throw new UnauthorizedException(userAlreadyDesactivated)
  }
}
