import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger'
import { API_DESCRIPTION, API_TITTLE, API_VERSION } from './config/constraints'
import { ValidationPipe } from '@nestjs/common'

async function bootstrap() {
  const app = await NestFactory.create(AppModule)

  const config = new DocumentBuilder()
    .setTitle(API_TITTLE)
    .setDescription(API_DESCRIPTION)
    .setVersion(API_VERSION)
    .build()
  const document = SwaggerModule.createDocument(app, config)
  SwaggerModule.setup('api', app, document)
  app.useGlobalPipes(new ValidationPipe())
  await app.listen(app.get('ConfigService').get('express.port'))
}
bootstrap()
