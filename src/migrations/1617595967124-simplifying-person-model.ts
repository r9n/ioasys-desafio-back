import { MigrationInterface, QueryRunner } from 'typeorm'

export class simplifyingPersonModel1617595967124 implements MigrationInterface {
  name = 'simplifyingPersonModel1617595967124'

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "person" DROP COLUMN "lastName"`)
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "person" ADD "lastName" character varying(36) NOT NULL`,
    )
  }
}
