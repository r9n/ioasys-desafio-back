import { MigrationInterface, QueryRunner } from 'typeorm'

export class changeGenreName1617579294364 implements MigrationInterface {
  name = 'changeGenreName1617579294364'

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "genre" RENAME COLUMN "title" TO "name"`,
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "genre" RENAME COLUMN "name" TO "title"`,
    )
  }
}
