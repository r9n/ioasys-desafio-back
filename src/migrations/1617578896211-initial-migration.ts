import { MigrationInterface, QueryRunner } from 'typeorm'

export class initialMigration1617578896211 implements MigrationInterface {
  name = 'initialMigration1617578896211'

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TYPE "person_type_enum" AS ENUM('actor', 'director', 'writer')`,
    )
    await queryRunner.query(
      `CREATE TABLE "person" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createDate" TIMESTAMP NOT NULL DEFAULT now(), "updateDate" TIMESTAMP NOT NULL DEFAULT now(), "version" integer NOT NULL DEFAULT '1', "name" character varying(36) NOT NULL, "lastName" character varying(36) NOT NULL, "type" "person_type_enum" NOT NULL, CONSTRAINT "PK_5fdaf670315c4b7e70cce85daa3" PRIMARY KEY ("id"))`,
    )
    await queryRunner.query(
      `CREATE TABLE "genre" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createDate" TIMESTAMP NOT NULL DEFAULT now(), "updateDate" TIMESTAMP NOT NULL DEFAULT now(), "version" integer NOT NULL DEFAULT '1', "title" character varying(16) NOT NULL, CONSTRAINT "PK_0285d4f1655d080cfcf7d1ab141" PRIMARY KEY ("id"))`,
    )
    await queryRunner.query(
      `CREATE TABLE "movie" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createDate" TIMESTAMP NOT NULL DEFAULT now(), "updateDate" TIMESTAMP NOT NULL DEFAULT now(), "version" integer NOT NULL DEFAULT '1', "title" character varying(36) NOT NULL, "description" character varying(320) NOT NULL, "genreId" uuid, CONSTRAINT "PK_cb3bb4d61cf764dc035cbedd422" PRIMARY KEY ("id"))`,
    )
    await queryRunner.query(
      `CREATE TABLE "rating" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createDate" TIMESTAMP NOT NULL DEFAULT now(), "updateDate" TIMESTAMP NOT NULL DEFAULT now(), "version" integer NOT NULL DEFAULT '1', "rating" integer NOT NULL, "userId" uuid, "movieId" uuid, CONSTRAINT "PK_ecda8ad32645327e4765b43649e" PRIMARY KEY ("id"))`,
    )
    await queryRunner.query(
      `CREATE TABLE "user" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createDate" TIMESTAMP NOT NULL DEFAULT now(), "updateDate" TIMESTAMP NOT NULL DEFAULT now(), "version" integer NOT NULL DEFAULT '1', "name" character varying(16) NOT NULL, "lastName" character varying(20) NOT NULL, "phoneNumber" character varying(18), "email" character varying(40) NOT NULL, "isActive" boolean NOT NULL DEFAULT true, "salt" character varying NOT NULL, "passwordHash" character varying NOT NULL, "roleId" uuid, CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`,
    )
    await queryRunner.query(
      `CREATE TABLE "role" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createDate" TIMESTAMP NOT NULL DEFAULT now(), "updateDate" TIMESTAMP NOT NULL DEFAULT now(), "version" integer NOT NULL DEFAULT '1', "role" character varying(16) NOT NULL, CONSTRAINT "PK_b36bcfe02fc8de3c57a8b2391c2" PRIMARY KEY ("id"))`,
    )
    await queryRunner.query(
      `CREATE TABLE "person_movies_movie" ("personId" uuid NOT NULL, "movieId" uuid NOT NULL, CONSTRAINT "PK_3343c2067740eb25e8b061f2993" PRIMARY KEY ("personId", "movieId"))`,
    )
    await queryRunner.query(
      `CREATE INDEX "IDX_4e580099fe8770307fc3266fed" ON "person_movies_movie" ("personId") `,
    )
    await queryRunner.query(
      `CREATE INDEX "IDX_e8b6ecfd58213164b570de3538" ON "person_movies_movie" ("movieId") `,
    )
    await queryRunner.query(
      `ALTER TABLE "movie" ADD CONSTRAINT "FK_3aaeb14b8d10d027190f3b159e5" FOREIGN KEY ("genreId") REFERENCES "genre"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    )
    await queryRunner.query(
      `ALTER TABLE "rating" ADD CONSTRAINT "FK_a6c53dfc89ba3188b389ef29a62" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    )
    await queryRunner.query(
      `ALTER TABLE "rating" ADD CONSTRAINT "FK_1a3badf27affbca3a224f01f7de" FOREIGN KEY ("movieId") REFERENCES "movie"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    )
    await queryRunner.query(
      `ALTER TABLE "user" ADD CONSTRAINT "FK_c28e52f758e7bbc53828db92194" FOREIGN KEY ("roleId") REFERENCES "role"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    )
    await queryRunner.query(
      `ALTER TABLE "person_movies_movie" ADD CONSTRAINT "FK_4e580099fe8770307fc3266fed8" FOREIGN KEY ("personId") REFERENCES "person"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    )
    await queryRunner.query(
      `ALTER TABLE "person_movies_movie" ADD CONSTRAINT "FK_e8b6ecfd58213164b570de35383" FOREIGN KEY ("movieId") REFERENCES "movie"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    )

    await queryRunner.query(` insert  into "role" (role) values('admin')`)
    await queryRunner.query(` insert  into "role" (role) values('user')`)
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "person_movies_movie" DROP CONSTRAINT "FK_e8b6ecfd58213164b570de35383"`,
    )
    await queryRunner.query(
      `ALTER TABLE "person_movies_movie" DROP CONSTRAINT "FK_4e580099fe8770307fc3266fed8"`,
    )
    await queryRunner.query(
      `ALTER TABLE "user" DROP CONSTRAINT "FK_c28e52f758e7bbc53828db92194"`,
    )
    await queryRunner.query(
      `ALTER TABLE "rating" DROP CONSTRAINT "FK_1a3badf27affbca3a224f01f7de"`,
    )
    await queryRunner.query(
      `ALTER TABLE "rating" DROP CONSTRAINT "FK_a6c53dfc89ba3188b389ef29a62"`,
    )
    await queryRunner.query(
      `ALTER TABLE "movie" DROP CONSTRAINT "FK_3aaeb14b8d10d027190f3b159e5"`,
    )
    await queryRunner.query(`DROP INDEX "IDX_e8b6ecfd58213164b570de3538"`)
    await queryRunner.query(`DROP INDEX "IDX_4e580099fe8770307fc3266fed"`)
    await queryRunner.query(`DROP TABLE "person_movies_movie"`)
    await queryRunner.query(`DROP TABLE "role"`)
    await queryRunner.query(`DROP TABLE "user"`)
    await queryRunner.query(`DROP TABLE "rating"`)
    await queryRunner.query(`DROP TABLE "movie"`)
    await queryRunner.query(`DROP TABLE "genre"`)
    await queryRunner.query(`DROP TABLE "person"`)
    await queryRunner.query(`DROP TYPE "person_type_enum"`)
  }
}
