import { MigrationInterface, QueryRunner } from 'typeorm'

export class addColumnDetailsToRating1617631724785
  implements MigrationInterface {
  name = 'addColumnDetailsToRating1617631724785'

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "rating" ADD "ratingDetails" character varying(512)`,
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "rating" DROP COLUMN "ratingDetails"`)
  }
}
