"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
const swagger_1 = require("@nestjs/swagger");
const constraints_1 = require("./config/constraints");
const common_1 = require("@nestjs/common");
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    const config = new swagger_1.DocumentBuilder()
        .setTitle(constraints_1.API_TITTLE)
        .setDescription(constraints_1.API_DESCRIPTION)
        .setVersion(constraints_1.API_VERSION)
        .build();
    const document = swagger_1.SwaggerModule.createDocument(app, config);
    swagger_1.SwaggerModule.setup('api', app, document);
    app.useGlobalPipes(new common_1.ValidationPipe());
    await app.listen(app.get('ConfigService').get('express.port'));
}
bootstrap();
//# sourceMappingURL=main.js.map