"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const path = require("path");
const typeorm_1 = require("@nestjs/typeorm");
const auth_module_1 = require("./modules/auth/auth.module");
const auth_service_1 = require("./modules/auth/auth.service");
const movie_module_1 = require("./modules/movie/movie.module");
const movie_service_1 = require("./modules/movie/movie.service");
const user_module_1 = require("./modules/user/user.module");
const user_service_1 = require("./modules/user/user.service");
const nestjs_config_1 = require("nestjs-config");
const role_module_1 = require("./modules/auth/roles/role.module");
const role_service_1 = require("./modules/auth/roles/role.service");
const person_controller_1 = require("./modules/person/person.controller");
const person_module_1 = require("./modules/person/person.module");
const rating_module_1 = require("./modules/rating/rating.module");
const movie_controller_1 = require("./modules/movie/movie.controller");
const user_controller_1 = require("./modules/user/user.controller");
const role_controller_1 = require("./modules/auth/roles/role.controller");
const genres_module_1 = require("./modules/movie/genres/genres.module");
const user_active_verify_middleware_1 = require("./middlewares/user-active-verify.middleware");
let AppModule = class AppModule {
    configure(consumer) {
        consumer
            .apply(user_active_verify_middleware_1.UserActiveValidation)
            .forRoutes('auth', 'genres', 'movie', 'person', 'user');
    }
};
AppModule = __decorate([
    common_1.Module({
        imports: [
            nestjs_config_1.ConfigModule.load(path.resolve(__dirname, 'config', '**/!(*.d).{ts,js}')),
            typeorm_1.TypeOrmModule.forRootAsync({
                useFactory: (config) => config.get('database'),
                inject: [nestjs_config_1.ConfigService],
            }),
            user_module_1.UserModule,
            auth_module_1.AuthModule,
            movie_module_1.MovieModule,
            person_module_1.PersonModule,
            role_module_1.RoleModule,
            rating_module_1.RatingModule,
            genres_module_1.GenresModule,
        ],
        controllers: [
            app_controller_1.AppController,
            person_controller_1.PersonController,
            movie_controller_1.MovieController,
            user_controller_1.UserController,
            role_controller_1.RoleController,
        ],
        providers: [app_service_1.AppService, auth_service_1.AuthService, movie_service_1.MovieService, user_service_1.UserService, role_service_1.RoleService],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map