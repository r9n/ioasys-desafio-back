"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const expressDefaultPort = 3000;
exports.default = {
    environment: process.env.NODE_ENV,
    port: process.env.PORT || expressDefaultPort,
    isProduction() {
        return this.get('express.environment') === 'production';
    },
};
//# sourceMappingURL=express.js.map