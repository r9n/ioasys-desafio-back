declare const _default: {
    environment: string;
    port: string | number;
    isProduction(): boolean;
};
export default _default;
