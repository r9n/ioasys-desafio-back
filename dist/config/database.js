"use strict";
function getMigrationDirectory() {
    const directory = process.env.NODE_ENV === 'migration' ? 'src' : `${__dirname}`;
    return `${directory}/migrations/**/*{.ts,.js}`;
}
const connectionOptions = {
    type: 'postgres',
    host: 'ioasys-postgres',
    username: 'postgres',
    password: '1234',
    database: 'postgres',
    entities: [`${__dirname}/../**/*.entity.{ts,js}`],
    synchronize: true,
    dropSchema: false,
    migrationsRun: false,
    logging: ['warn', 'error'],
    logger: 'file',
    migrations: [getMigrationDirectory()],
    cli: {
        migrationsDir: 'src/migrations',
    },
};
module.exports = connectionOptions;
//# sourceMappingURL=database.js.map