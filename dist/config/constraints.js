"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.maxItemsPerPage = exports.MIN_GENRE_NAME_LENGHT = exports.MAX_GENRE_NAME_LENGHT = exports.passwordRegex = exports.MIN_RATING_DETAILS_LENGHT = exports.MAX_RATING_DETAILS_LENGHT = exports.MIN_RATING = exports.MAX_RATING = exports.MIN_ROLE_NAME_LENGHT = exports.MAX_ROLE_NAME_LENGHT = exports.MIN_PERSON_NAME_LENGHT = exports.MAX_PERSON_NAME_LENGHT = exports.MIN_EMAIL_LENGHT = exports.MAX_EMAIL_LENGHT = exports.MIN_PHONE_LENGHT = exports.MAX_PHONE_LENGHT = exports.MIN_USER_LAST_NAME_LENGHT = exports.MAX_USER_LAST_NAME_LENGHT = exports.MIN_USER_NAME_LENGHT = exports.MAX_USER_NAME_LENGHT = exports.MIN_MOVIE_DESCRIPTION_LENGHT = exports.MAX_MOVIE_DESCRIPTION_LENGHT = exports.MIN_MOVIE_NAME_LENGHT = exports.MAX_MOVIE_NAME_LENGHT = exports.API_VERSION = exports.API_DESCRIPTION = exports.API_TITTLE = exports.API_NAME = void 0;
exports.API_NAME = 'IoasysAPI-IMDB';
exports.API_TITTLE = 'IOASYS Swagger Documentation';
exports.API_DESCRIPTION = ' Uma api para o gerenciamento de usuários e filmes';
exports.API_VERSION = '1.0';
exports.MAX_MOVIE_NAME_LENGHT = 36;
exports.MIN_MOVIE_NAME_LENGHT = 36;
exports.MAX_MOVIE_DESCRIPTION_LENGHT = 320;
exports.MIN_MOVIE_DESCRIPTION_LENGHT = 20;
exports.MAX_USER_NAME_LENGHT = 16;
exports.MIN_USER_NAME_LENGHT = 4;
exports.MAX_USER_LAST_NAME_LENGHT = 20;
exports.MIN_USER_LAST_NAME_LENGHT = 2;
exports.MAX_PHONE_LENGHT = 18;
exports.MIN_PHONE_LENGHT = 8;
exports.MAX_EMAIL_LENGHT = 40;
exports.MIN_EMAIL_LENGHT = 7;
exports.MAX_PERSON_NAME_LENGHT = 36;
exports.MIN_PERSON_NAME_LENGHT = 4;
exports.MAX_ROLE_NAME_LENGHT = 16;
exports.MIN_ROLE_NAME_LENGHT = 3;
exports.MAX_RATING = 4;
exports.MIN_RATING = 0;
exports.MAX_RATING_DETAILS_LENGHT = 512;
exports.MIN_RATING_DETAILS_LENGHT = 0;
exports.passwordRegex = '/((?=.*d)|(?=.*W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/';
exports.MAX_GENRE_NAME_LENGHT = 16;
exports.MIN_GENRE_NAME_LENGHT = 3;
exports.maxItemsPerPage = 25;
//# sourceMappingURL=constraints.js.map