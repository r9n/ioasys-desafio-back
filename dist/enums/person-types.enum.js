"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PersonTypes = void 0;
var PersonTypes;
(function (PersonTypes) {
    PersonTypes["ACTOR"] = "actor";
    PersonTypes["DIRECTOR"] = "director";
    PersonTypes["WRITER"] = "writer";
})(PersonTypes = exports.PersonTypes || (exports.PersonTypes = {}));
//# sourceMappingURL=person-types.enum.js.map