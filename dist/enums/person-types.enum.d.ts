export declare enum PersonTypes {
    ACTOR = "actor",
    DIRECTOR = "director",
    WRITER = "writer"
}
