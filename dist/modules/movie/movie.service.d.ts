import { Rating } from '../rating/rating.entity';
import { RatingService } from '../rating/rating.service';
import { User } from '../user/user.entity';
import { FilterMovieDto } from './dto/filter-movie.dto';
import { GetOneMovieDto } from './dto/get-one.dto';
import { PaginatedGetMovieDto } from './dto/paginated-list.dto';
import { RegisterMovieDto } from './dto/register-movie.dto';
import { VoteMovieDto } from './dto/vote-movie.dto';
import { GenresService } from './genres/genres.service';
import { Movie } from './movie.entity';
import { MovieRepositoy } from './movie.repository';
export declare class MovieService {
    private movieRepository;
    private ratingService;
    private geneService;
    constructor(movieRepository: MovieRepositoy, ratingService: RatingService, geneService: GenresService);
    registerMovie(user: User, dto: RegisterMovieDto): Promise<Movie>;
    rate(user: User, dto: VoteMovieDto, movieId: string): Promise<Rating>;
    getAll(filterDto: FilterMovieDto, page: number): Promise<PaginatedGetMovieDto>;
    getOneMovie(movieId: string): Promise<GetOneMovieDto>;
}
