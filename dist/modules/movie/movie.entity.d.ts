import { BaseEntity } from 'src/utils/entities/base-entity';
import { Person } from '../person/person.entity';
import { Rating } from '../rating/rating.entity';
import { RegisterMovieDto } from './dto/register-movie.dto';
import { Genre } from './genres/genres.entity';
export declare class Movie extends BaseEntity {
    title: string;
    description: string;
    ratings: Array<Rating>;
    participants: Array<Person>;
    genre: Genre;
    constructor(partial: Partial<Movie>);
    fromDto(dto: RegisterMovieDto): void;
}
