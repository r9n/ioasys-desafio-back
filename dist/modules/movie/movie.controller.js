"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MovieController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const exceptions_message_1 = require("../../utils/messages/exceptions.message");
const forbidden_message_1 = require("../../utils/messages/forbidden.message");
const get_users_decorator_1 = require("../auth/decorators/get-users.decorator");
const user_entity_1 = require("../user/user.entity");
const register_movie_dto_1 = require("./dto/register-movie.dto");
const movie_entity_1 = require("./movie.entity");
const passport_1 = require("@nestjs/passport");
const movie_service_1 = require("./movie.service");
const vote_movie_dto_1 = require("./dto/vote-movie.dto");
const paginated_list_dto_1 = require("./dto/paginated-list.dto");
const filter_movie_dto_1 = require("./dto/filter-movie.dto");
const get_one_dto_1 = require("./dto/get-one.dto");
let MovieController = class MovieController {
    constructor(movieService) {
        this.movieService = movieService;
    }
    async registerMovie(user, dto) {
        return this.movieService.registerMovie(user, dto);
    }
    async rateMovie(user, dto, movieId) {
        return this.movieService.rate(user, dto, movieId);
    }
    async getAll(dto, page) {
        return this.movieService.getAll(dto, page);
    }
    async getOne(movieId) {
        return this.movieService.getOneMovie(movieId);
    }
};
__decorate([
    common_1.Post(),
    swagger_1.ApiCreatedResponse({ type: movie_entity_1.Movie }),
    swagger_1.ApiForbiddenResponse({ description: forbidden_message_1.forbiddenRegister('movie') }),
    swagger_1.ApiBadRequestResponse({ description: exceptions_message_1.movieAlreadyRegistered }),
    swagger_1.ApiNotFoundResponse({
        description: `${JSON.stringify(exceptions_message_1.participantNotFound)},
                ${JSON.stringify(exceptions_message_1.objectDoesNotExistsMessage('genre'))}`,
    }),
    __param(0, get_users_decorator_1.GetUser()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_entity_1.User,
        register_movie_dto_1.RegisterMovieDto]),
    __metadata("design:returntype", Promise)
], MovieController.prototype, "registerMovie", null);
__decorate([
    common_1.Post(':id/vote'),
    swagger_1.ApiCreatedResponse(),
    swagger_1.ApiForbiddenResponse({ description: forbidden_message_1.forbiddenRate }),
    swagger_1.ApiNotFoundResponse({ description: exceptions_message_1.objectDoesNotExistsMessage('movie') }),
    __param(0, get_users_decorator_1.GetUser()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_entity_1.User,
        vote_movie_dto_1.VoteMovieDto, String]),
    __metadata("design:returntype", Promise)
], MovieController.prototype, "rateMovie", null);
__decorate([
    common_1.Post('/getall/:page'),
    swagger_1.ApiOkResponse({ type: paginated_list_dto_1.PaginatedGetMovieDto }),
    swagger_1.ApiBadRequestResponse({ description: exceptions_message_1.filterInvalid }),
    __param(0, common_1.Body()),
    __param(1, common_1.Param('page')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [filter_movie_dto_1.FilterMovieDto, Number]),
    __metadata("design:returntype", Promise)
], MovieController.prototype, "getAll", null);
__decorate([
    common_1.Get(':id'),
    swagger_1.ApiOkResponse({ type: get_one_dto_1.GetOneMovieDto }),
    swagger_1.ApiNotFoundResponse({ description: exceptions_message_1.objectDoesNotExistsMessage('movie') }),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], MovieController.prototype, "getOne", null);
MovieController = __decorate([
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    swagger_1.ApiBearerAuth(),
    common_1.Controller('movie'),
    __metadata("design:paramtypes", [movie_service_1.MovieService])
], MovieController);
exports.MovieController = MovieController;
//# sourceMappingURL=movie.controller.js.map