"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MovieService = void 0;
const common_1 = require("@nestjs/common");
const constraints_1 = require("../../config/constraints");
const postgres_erros_enum_1 = require("../../enums/postgres-erros.enum");
const roles_enum_1 = require("../../enums/roles.enum");
const exceptions_message_1 = require("../../utils/messages/exceptions.message");
const forbidden_message_1 = require("../../utils/messages/forbidden.message");
const rating_entity_1 = require("../rating/rating.entity");
const rating_service_1 = require("../rating/rating.service");
const paginated_list_dto_1 = require("./dto/paginated-list.dto");
const genres_service_1 = require("./genres/genres.service");
const movie_entity_1 = require("./movie.entity");
const movie_repository_1 = require("./movie.repository");
let MovieService = class MovieService {
    constructor(movieRepository, ratingService, geneService) {
        this.movieRepository = movieRepository;
        this.ratingService = ratingService;
        this.geneService = geneService;
    }
    async registerMovie(user, dto) {
        if (user.role.role !== roles_enum_1.Roles.ADMIN) {
            throw new common_1.ForbiddenException(forbidden_message_1.forbiddenRegister);
        }
        const isAlreadyRegistered = await this.movieRepository.findOne({
            title: dto.title,
            description: dto.description,
        });
        if (isAlreadyRegistered) {
            throw new common_1.BadRequestException(exceptions_message_1.movieAlreadyRegistered);
        }
        const genre = await this.geneService.getGenreByName(dto.genre);
        const newMovie = new movie_entity_1.Movie({});
        newMovie.fromDto(dto);
        newMovie.genre = genre;
        return this.movieRepository.save(newMovie).catch((error) => {
            switch (error.code) {
                case postgres_erros_enum_1.PostgresErroCodes.NOTFOUND: {
                    throw new common_1.NotFoundException(exceptions_message_1.participantNotFound);
                }
                default: {
                    throw new common_1.InternalServerErrorException(error.message);
                }
            }
        });
    }
    async rate(user, dto, movieId) {
        if (user.role.role !== roles_enum_1.Roles.USER) {
            throw new common_1.ForbiddenException(forbidden_message_1.forbiddenRate);
        }
        const movie = await this.movieRepository.findOne(movieId);
        if (!movie) {
            throw new common_1.NotFoundException(exceptions_message_1.objectDoesNotExistsMessage('movie'));
        }
        const rating = new rating_entity_1.Rating({
            rating: dto.grade,
            ratingDetails: dto.ratingDetails,
            user,
            movie,
        });
        return this.ratingService.save(rating).catch((error) => {
            throw new common_1.InternalServerErrorException(error.message);
        });
    }
    async getAll(filterDto, page) {
        const dto = new paginated_list_dto_1.PaginatedGetMovieDto();
        if (page < 0) {
            page = 0;
        }
        const offset = (page - 1) * constraints_1.maxItemsPerPage;
        const movies = await this.movieRepository.getAllMovesByFilter(filterDto, offset);
        dto.page = page;
        dto.total = movies.length;
        dto.pageCount = movies.length;
        dto.data = movies;
        return dto;
    }
    async getOneMovie(movieId) {
        return this.movieRepository.getOneMovie(movieId);
    }
};
MovieService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [movie_repository_1.MovieRepositoy,
        rating_service_1.RatingService,
        genres_service_1.GenresService])
], MovieService);
exports.MovieService = MovieService;
//# sourceMappingURL=movie.service.js.map