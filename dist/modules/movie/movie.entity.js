"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Movie = void 0;
const swagger_1 = require("@nestjs/swagger");
const constraints_1 = require("../../config/constraints");
const base_entity_1 = require("../../utils/entities/base-entity");
const typeorm_1 = require("typeorm");
const person_entity_1 = require("../person/person.entity");
const rating_entity_1 = require("../rating/rating.entity");
const genres_entity_1 = require("./genres/genres.entity");
let Movie = class Movie extends base_entity_1.BaseEntity {
    constructor(partial) {
        super();
        Object.assign(this, partial);
    }
    fromDto(dto) {
        this.title = dto.title;
        this.description = dto.description;
        this.ratings = dto.ratings;
        this.participants = dto.participants;
    }
};
__decorate([
    typeorm_1.Column({ type: 'varchar', length: constraints_1.MAX_MOVIE_NAME_LENGHT, nullable: false }),
    swagger_1.ApiProperty({
        type: String,
        maxLength: constraints_1.MAX_MOVIE_NAME_LENGHT,
        minLength: constraints_1.MIN_MOVIE_NAME_LENGHT,
        required: true,
    }),
    __metadata("design:type", String)
], Movie.prototype, "title", void 0);
__decorate([
    typeorm_1.Column({
        type: 'varchar',
        length: constraints_1.MAX_MOVIE_DESCRIPTION_LENGHT,
        nullable: false,
    }),
    swagger_1.ApiProperty({
        type: String,
        maxLength: constraints_1.MAX_MOVIE_DESCRIPTION_LENGHT,
        minLength: constraints_1.MIN_MOVIE_DESCRIPTION_LENGHT,
        required: false,
    }),
    __metadata("design:type", String)
], Movie.prototype, "description", void 0);
__decorate([
    typeorm_1.OneToMany(() => rating_entity_1.Rating, (ratings) => ratings.movie),
    __metadata("design:type", Array)
], Movie.prototype, "ratings", void 0);
__decorate([
    typeorm_1.ManyToMany(() => person_entity_1.Person, (person) => person.movies),
    typeorm_1.JoinColumn(),
    __metadata("design:type", Array)
], Movie.prototype, "participants", void 0);
__decorate([
    typeorm_1.ManyToOne(() => genres_entity_1.Genre, (genre) => genre.movie),
    __metadata("design:type", genres_entity_1.Genre)
], Movie.prototype, "genre", void 0);
Movie = __decorate([
    typeorm_1.Entity('movie'),
    __metadata("design:paramtypes", [Object])
], Movie);
exports.Movie = Movie;
//# sourceMappingURL=movie.entity.js.map