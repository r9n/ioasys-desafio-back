import { User } from '../user/user.entity';
import { RegisterMovieDto } from './dto/register-movie.dto';
import { Movie } from './movie.entity';
import { MovieService } from './movie.service';
import { VoteMovieDto } from './dto/vote-movie.dto';
import { Rating } from '../rating/rating.entity';
import { PaginatedGetMovieDto } from './dto/paginated-list.dto';
import { FilterMovieDto } from './dto/filter-movie.dto';
import { GetOneMovieDto } from './dto/get-one.dto';
export declare class MovieController {
    private movieService;
    constructor(movieService: MovieService);
    registerMovie(user: User, dto: RegisterMovieDto): Promise<Movie>;
    rateMovie(user: User, dto: VoteMovieDto, movieId: string): Promise<Rating>;
    getAll(dto: FilterMovieDto, page: number): Promise<PaginatedGetMovieDto>;
    getOne(movieId: string): Promise<GetOneMovieDto>;
}
