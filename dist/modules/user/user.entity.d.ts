import { BaseEntity } from 'src/utils/entities/base-entity';
import { Rating } from '../rating/rating.entity';
import { Role } from '../auth/roles/role.entity';
import { EditUserDto } from './dto/edit-user.dto';
export declare class User extends BaseEntity {
    name: string;
    lastName: string;
    phoneNumber: string;
    email: string;
    isActive: boolean;
    ratings: Array<Rating>;
    role: Role;
    salt: string;
    passwordHash: string;
    constructor(partial: Partial<User>);
    validationPassword(password: string): Promise<boolean>;
    fromDto(dto: EditUserDto): void;
}
