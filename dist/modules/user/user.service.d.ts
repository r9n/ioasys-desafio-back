import { SigninDto } from '../auth/dto/signin.dto';
import { EditUserDto } from './dto/edit-user.dto';
import { User } from './user.entity';
import { UserRepository } from './user.repository';
export declare class UserService {
    private userRepository;
    constructor(userRepository: UserRepository);
    save(user: User): Promise<User>;
    findUserByEmail(email: string): Promise<User>;
    validateUserPassword(dto: SigninDto): Promise<boolean>;
    editUser(requestUser: User, dto: EditUserDto, userId: string): Promise<User>;
    deactivateUser(requestUser: User, userId: string): Promise<User>;
    reactivateUser(user: User): Promise<User>;
}
