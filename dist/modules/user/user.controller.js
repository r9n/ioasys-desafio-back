"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const forbidden_message_1 = require("../../utils/messages/forbidden.message");
const get_users_decorator_1 = require("../auth/decorators/get-users.decorator");
const edit_user_dto_1 = require("./dto/edit-user.dto");
const user_entity_1 = require("./user.entity");
const passport_1 = require("@nestjs/passport");
const user_service_1 = require("./user.service");
const signin_response_dto_1 = require("../auth/dto/signin-response.dto");
const signin_dto_1 = require("../auth/dto/signin.dto");
const exceptions_message_1 = require("../../utils/messages/exceptions.message");
let UserController = class UserController {
    constructor(userSerivce) {
        this.userSerivce = userSerivce;
    }
    async editUser(user, dto, userId) {
        return this.userSerivce.editUser(user, dto, userId);
    }
    async deactivateUser(user, userId) {
        return this.userSerivce.deactivateUser(user, userId);
    }
    async reactivate(user) {
        return this.userSerivce.reactivateUser(user);
    }
};
__decorate([
    common_1.Patch(':id/edit'),
    swagger_1.ApiForbiddenResponse({ description: forbidden_message_1.editForbidden }),
    swagger_1.ApiNotFoundResponse({ description: exceptions_message_1.objectDoesNotExistsMessage('user') }),
    swagger_1.ApiCreatedResponse({ type: user_entity_1.User }),
    __param(0, get_users_decorator_1.GetUser()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_entity_1.User,
        edit_user_dto_1.EditUserDto, String]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "editUser", null);
__decorate([
    common_1.Patch(':id/deactivate'),
    swagger_1.ApiForbiddenResponse({ description: forbidden_message_1.editForbidden }),
    swagger_1.ApiNotFoundResponse({ description: exceptions_message_1.objectDoesNotExistsMessage('user') }),
    swagger_1.ApiBadRequestResponse({ description: forbidden_message_1.forbiddenDeactivate }),
    swagger_1.ApiCreatedResponse({ type: user_entity_1.User }),
    __param(0, get_users_decorator_1.GetUser()),
    __param(1, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_entity_1.User, String]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "deactivateUser", null);
__decorate([
    common_1.Post(':id/reactivate'),
    swagger_1.ApiForbiddenResponse({
        description: forbidden_message_1.forbiddenToReactivate,
    }),
    swagger_1.ApiNotFoundResponse({ description: exceptions_message_1.objectDoesNotExistsMessage('user') }),
    swagger_1.ApiBody({ type: signin_dto_1.SigninDto }),
    swagger_1.ApiCreatedResponse({ type: signin_response_dto_1.SigninResponseDto }),
    __param(0, get_users_decorator_1.GetUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_entity_1.User]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "reactivate", null);
UserController = __decorate([
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    swagger_1.ApiBearerAuth(),
    common_1.Controller('user'),
    __metadata("design:paramtypes", [user_service_1.UserService])
], UserController);
exports.UserController = UserController;
//# sourceMappingURL=user.controller.js.map