import { EditUserDto } from './dto/edit-user.dto';
import { User } from './user.entity';
import { UserService } from './user.service';
export declare class UserController {
    private userSerivce;
    constructor(userSerivce: UserService);
    editUser(user: User, dto: EditUserDto, userId: string): Promise<User>;
    deactivateUser(user: User, userId: string): Promise<User>;
    reactivate(user: User): Promise<User>;
}
