"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const roles_enum_1 = require("../../enums/roles.enum");
const exceptions_message_1 = require("../../utils/messages/exceptions.message");
const forbidden_message_1 = require("../../utils/messages/forbidden.message");
const user_repository_1 = require("./user.repository");
let UserService = class UserService {
    constructor(userRepository) {
        this.userRepository = userRepository;
    }
    async save(user) {
        return this.userRepository.save(user);
    }
    async findUserByEmail(email) {
        return this.userRepository.findOne({ email }, { relations: ['role'] });
    }
    async validateUserPassword(dto) {
        const { email, password: password } = dto;
        const user = await this.userRepository.findOne({ email });
        return await user.validationPassword(password);
    }
    async editUser(requestUser, dto, userId) {
        if (requestUser.id !== userId && requestUser.role.role !== roles_enum_1.Roles.ADMIN) {
            throw new common_1.ForbiddenException(forbidden_message_1.editForbidden);
        }
        const databaseUser = await this.userRepository.findOne({ id: userId });
        if (!databaseUser) {
            throw new common_1.NotFoundException(exceptions_message_1.objectDoesNotExistsMessage('user'));
        }
        databaseUser.fromDto(dto);
        return this.userRepository.save(databaseUser).catch((error) => {
            throw new common_1.InternalServerErrorException(error.message);
        });
    }
    async deactivateUser(requestUser, userId) {
        const user = await this.userRepository.findOne({ id: userId });
        if (!user) {
            throw new common_1.NotFoundException(exceptions_message_1.objectDoesNotExistsMessage('user'));
        }
        if (requestUser.id !== userId) {
            throw new common_1.ForbiddenException(forbidden_message_1.forbiddenDeactivate);
        }
        if (!user.isActive) {
            throw new common_1.BadRequestException(exceptions_message_1.userAlreadyDesactivated);
        }
        user.isActive = false;
        return this.userRepository.save(user).catch((error) => {
            throw new common_1.InternalServerErrorException(error.message);
        });
    }
    async reactivateUser(user) {
        if (!user) {
            throw new common_1.NotFoundException(exceptions_message_1.objectDoesNotExistsMessage('user'));
        }
        if (user.role.role !== roles_enum_1.Roles.ADMIN) {
            throw new common_1.ForbiddenException(forbidden_message_1.forbiddenToReactivate);
        }
        if (!user.isActive) {
            throw new common_1.BadRequestException(exceptions_message_1.userAlreadyDesactivated);
        }
        user.isActive = true;
        return this.userRepository.save(user).catch((error) => {
            throw new common_1.InternalServerErrorException(error.message);
        });
    }
};
UserService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(user_repository_1.UserRepository)),
    __metadata("design:paramtypes", [user_repository_1.UserRepository])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map