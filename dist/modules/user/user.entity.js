"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
const constraints_1 = require("../../config/constraints");
const base_entity_1 = require("../../utils/entities/base-entity");
const typeorm_1 = require("typeorm");
const rating_entity_1 = require("../rating/rating.entity");
const role_entity_1 = require("../auth/roles/role.entity");
const swagger_1 = require("@nestjs/swagger");
const bcrypt = require("bcrypt");
let User = class User extends base_entity_1.BaseEntity {
    constructor(partial) {
        super();
        Object.assign(this, partial);
    }
    async validationPassword(password) {
        const hash = await bcrypt.hash(password, this.salt);
        return hash === this.passwordHash;
    }
    fromDto(dto) {
        this.name = dto.name ? dto.name : this.name;
        this.lastName = dto.lastName ? dto.lastName : this.lastName;
        this.email = dto.email ? dto.email : this.email;
        this.phoneNumber = dto.phoneNumber ? dto.phoneNumber : this.phoneNumber;
    }
};
__decorate([
    typeorm_1.Column({ type: 'varchar', length: constraints_1.MAX_USER_NAME_LENGHT, nullable: false }),
    swagger_1.ApiProperty({
        type: String,
        maxLength: constraints_1.MAX_USER_NAME_LENGHT,
        minLength: constraints_1.MIN_USER_NAME_LENGHT,
        required: true,
    }),
    __metadata("design:type", String)
], User.prototype, "name", void 0);
__decorate([
    typeorm_1.Column({
        type: 'varchar',
        length: constraints_1.MAX_USER_LAST_NAME_LENGHT,
        nullable: false,
    }),
    swagger_1.ApiProperty({
        type: String,
        maxLength: constraints_1.MAX_USER_LAST_NAME_LENGHT,
        minLength: constraints_1.MIN_USER_LAST_NAME_LENGHT,
        required: true,
    }),
    __metadata("design:type", String)
], User.prototype, "lastName", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', length: constraints_1.MAX_PHONE_LENGHT, nullable: true }),
    swagger_1.ApiProperty({
        type: String,
        maxLength: constraints_1.MAX_PHONE_LENGHT,
        minLength: constraints_1.MIN_PHONE_LENGHT,
        required: false,
    }),
    __metadata("design:type", String)
], User.prototype, "phoneNumber", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', length: constraints_1.MAX_EMAIL_LENGHT, nullable: false }),
    swagger_1.ApiProperty({
        type: String,
        maxLength: constraints_1.MAX_EMAIL_LENGHT,
        minLength: constraints_1.MIN_EMAIL_LENGHT,
        required: true,
    }),
    __metadata("design:type", String)
], User.prototype, "email", void 0);
__decorate([
    typeorm_1.Column({ type: 'boolean', default: true }),
    swagger_1.ApiProperty({ type: Boolean }),
    __metadata("design:type", Boolean)
], User.prototype, "isActive", void 0);
__decorate([
    typeorm_1.OneToMany(() => rating_entity_1.Rating, (ratings) => ratings.user),
    __metadata("design:type", Array)
], User.prototype, "ratings", void 0);
__decorate([
    typeorm_1.ManyToOne(() => role_entity_1.Role, (role) => role.user),
    __metadata("design:type", role_entity_1.Role)
], User.prototype, "role", void 0);
__decorate([
    typeorm_1.Column({ select: false }),
    __metadata("design:type", String)
], User.prototype, "salt", void 0);
__decorate([
    typeorm_1.Column({ select: false }),
    __metadata("design:type", String)
], User.prototype, "passwordHash", void 0);
User = __decorate([
    typeorm_1.Entity(),
    __metadata("design:paramtypes", [Object])
], User);
exports.User = User;
//# sourceMappingURL=user.entity.js.map