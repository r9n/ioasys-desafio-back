import { User } from '../user/user.entity';
import { RegisterPersonDto } from './dto/person-register.dto';
import { Person } from './person.entity';
import { PersonService } from './person.service';
export declare class PersonController {
    private personService;
    constructor(personService: PersonService);
    register(user: User, dto: RegisterPersonDto): Promise<Person>;
}
