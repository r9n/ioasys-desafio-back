"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PersonController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const exceptions_message_1 = require("../../utils/messages/exceptions.message");
const forbidden_message_1 = require("../../utils/messages/forbidden.message");
const get_users_decorator_1 = require("../auth/decorators/get-users.decorator");
const user_entity_1 = require("../user/user.entity");
const person_register_dto_1 = require("./dto/person-register.dto");
const person_service_1 = require("./person.service");
const passport_1 = require("@nestjs/passport");
let PersonController = class PersonController {
    constructor(personService) {
        this.personService = personService;
    }
    async register(user, dto) {
        return this.personService.register(user, dto);
    }
};
__decorate([
    common_1.Post(),
    swagger_1.ApiBadRequestResponse({ description: exceptions_message_1.objectAlreadyExistsMessage('person') }),
    swagger_1.ApiForbiddenResponse({ description: forbidden_message_1.forbiddenRegister('new persons') }),
    __param(0, get_users_decorator_1.GetUser()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_entity_1.User,
        person_register_dto_1.RegisterPersonDto]),
    __metadata("design:returntype", Promise)
], PersonController.prototype, "register", null);
PersonController = __decorate([
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    swagger_1.ApiBearerAuth(),
    common_1.Controller('person'),
    __metadata("design:paramtypes", [person_service_1.PersonService])
], PersonController);
exports.PersonController = PersonController;
//# sourceMappingURL=person.controller.js.map