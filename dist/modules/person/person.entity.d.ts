import { PersonTypes } from 'src/enums/person-types.enum';
import { BaseEntity } from 'src/utils/entities/base-entity';
import { Movie } from '../movie/movie.entity';
import { RegisterPersonDto } from './dto/person-register.dto';
export declare class Person extends BaseEntity {
    name: string;
    type: PersonTypes;
    movies: Array<Movie>;
    constructor(partial: Partial<Person>);
    fromoDto(dto: RegisterPersonDto): void;
}
