"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Person = void 0;
const swagger_1 = require("@nestjs/swagger");
const constraints_1 = require("../../config/constraints");
const person_types_enum_1 = require("../../enums/person-types.enum");
const base_entity_1 = require("../../utils/entities/base-entity");
const typeorm_1 = require("typeorm");
const movie_entity_1 = require("../movie/movie.entity");
let Person = class Person extends base_entity_1.BaseEntity {
    constructor(partial) {
        super();
        Object.assign(this, partial);
    }
    fromoDto(dto) {
        this.name = dto.name;
        this.type = dto.type;
        this.movies = dto.movies;
    }
};
__decorate([
    typeorm_1.Column({ type: 'varchar', length: constraints_1.MAX_PERSON_NAME_LENGHT, nullable: false }),
    swagger_1.ApiProperty({
        type: String,
        maxLength: constraints_1.MAX_PERSON_NAME_LENGHT,
        required: true,
    }),
    __metadata("design:type", String)
], Person.prototype, "name", void 0);
__decorate([
    typeorm_1.Column({ type: 'enum', enum: person_types_enum_1.PersonTypes }),
    swagger_1.ApiProperty({ type: person_types_enum_1.PersonTypes, required: true, nullable: false }),
    __metadata("design:type", String)
], Person.prototype, "type", void 0);
__decorate([
    typeorm_1.ManyToMany(() => movie_entity_1.Movie, (movie) => movie.participants),
    __metadata("design:type", Array)
], Person.prototype, "movies", void 0);
Person = __decorate([
    typeorm_1.Entity(),
    __metadata("design:paramtypes", [Object])
], Person);
exports.Person = Person;
//# sourceMappingURL=person.entity.js.map