"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PersonService = void 0;
const common_1 = require("@nestjs/common");
const roles_enum_1 = require("../../enums/roles.enum");
const exceptions_message_1 = require("../../utils/messages/exceptions.message");
const forbidden_message_1 = require("../../utils/messages/forbidden.message");
const person_entity_1 = require("./person.entity");
const person_repository_1 = require("./person.repository");
let PersonService = class PersonService {
    constructor(personRepository) {
        this.personRepository = personRepository;
    }
    async register(user, dto) {
        const databasePerson = await this.personRepository.findOne({
            name: dto.name,
            type: dto.type,
        });
        if (databasePerson) {
            throw new common_1.BadRequestException(exceptions_message_1.objectAlreadyExistsMessage(dto.type.toString()));
        }
        if (user.role.role !== roles_enum_1.Roles.ADMIN) {
            throw new common_1.ForbiddenException(forbidden_message_1.forbiddenRegister('new persons'));
        }
        const newPerson = new person_entity_1.Person({});
        newPerson.fromoDto(dto);
        return this.personRepository.save(newPerson).catch((error) => {
            throw new common_1.InternalServerErrorException(error.message);
        });
    }
};
PersonService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [person_repository_1.PersonRepository])
], PersonService);
exports.PersonService = PersonService;
//# sourceMappingURL=person.service.js.map