import { User } from '../user/user.entity';
import { RegisterPersonDto } from './dto/person-register.dto';
import { Person } from './person.entity';
import { PersonRepository } from './person.repository';
export declare class PersonService {
    private personRepository;
    constructor(personRepository: PersonRepository);
    register(user: User, dto: RegisterPersonDto): Promise<Person>;
}
