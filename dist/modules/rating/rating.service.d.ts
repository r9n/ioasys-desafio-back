import { Rating } from './rating.entity';
import { RatingRepository } from './rating.repository';
export declare class RatingService {
    private ratingRepository;
    constructor(ratingRepository: RatingRepository);
    save(rating: Rating): Promise<Rating>;
}
