import { BaseEntity } from 'src/utils/entities/base-entity';
import { User } from '../user/user.entity';
import { Movie } from '../movie/movie.entity';
export declare class Rating extends BaseEntity {
    rating: number;
    ratingDetails: string;
    user: User;
    movie: Movie;
    constructor(partial: Partial<Rating>);
}
