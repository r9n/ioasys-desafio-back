"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Rating = void 0;
const base_entity_1 = require("../../utils/entities/base-entity");
const typeorm_1 = require("typeorm");
const class_validator_1 = require("class-validator");
const constraints_1 = require("../../config/constraints");
const user_entity_1 = require("../user/user.entity");
const movie_entity_1 = require("../movie/movie.entity");
const swagger_1 = require("@nestjs/swagger");
let Rating = class Rating extends base_entity_1.BaseEntity {
    constructor(partial) {
        super();
        Object.assign(this, partial);
    }
};
__decorate([
    typeorm_1.Column({ type: Number }),
    class_validator_1.Min(constraints_1.MIN_RATING),
    class_validator_1.Max(constraints_1.MAX_RATING),
    swagger_1.ApiProperty({
        type: Number,
        minimum: constraints_1.MIN_RATING,
        maximum: constraints_1.MAX_RATING,
        required: true,
    }),
    __metadata("design:type", Number)
], Rating.prototype, "rating", void 0);
__decorate([
    typeorm_1.Column({
        type: 'varchar',
        length: constraints_1.MAX_RATING_DETAILS_LENGHT,
        nullable: true,
    }),
    swagger_1.ApiProperty({
        type: String,
        maxLength: constraints_1.MAX_RATING_DETAILS_LENGHT,
        minLength: constraints_1.MIN_RATING_DETAILS_LENGHT,
    }),
    __metadata("design:type", String)
], Rating.prototype, "ratingDetails", void 0);
__decorate([
    typeorm_1.ManyToOne(() => user_entity_1.User, (user) => user.ratings),
    __metadata("design:type", user_entity_1.User)
], Rating.prototype, "user", void 0);
__decorate([
    typeorm_1.ManyToOne(() => movie_entity_1.Movie, (movie) => movie.ratings),
    __metadata("design:type", movie_entity_1.Movie)
], Rating.prototype, "movie", void 0);
Rating = __decorate([
    typeorm_1.Entity(),
    __metadata("design:paramtypes", [Object])
], Rating);
exports.Rating = Rating;
//# sourceMappingURL=rating.entity.js.map