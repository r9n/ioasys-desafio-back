import { AuthService } from './auth.service';
import { SigninResponseDto } from './dto/signin-response.dto';
import { SigninDto } from './dto/signin.dto';
import { SingupResponseDto } from './dto/signup-response.dto';
import { SignupDto } from './dto/signup.dto';
export declare class AuthController {
    private authService;
    constructor(authService: AuthService);
    signup(dto: SignupDto): Promise<SingupResponseDto>;
    singin(dto: SigninDto): Promise<SigninResponseDto>;
}
