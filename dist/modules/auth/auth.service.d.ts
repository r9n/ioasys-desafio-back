import { UserService } from '../user/user.service';
import { SigninResponseDto } from './dto/signin-response.dto';
import { SignupDto } from './dto/signup.dto';
import { SigninDto } from './dto/signin.dto';
import { SingupResponseDto } from './dto/signup-response.dto';
import { RoleService } from './roles/role.service';
import { JwtService } from '@nestjs/jwt';
export declare class AuthService {
    private userService;
    private rolesService;
    private jwtService;
    constructor(userService: UserService, rolesService: RoleService, jwtService: JwtService);
    signup(dto: SignupDto): Promise<SingupResponseDto>;
    signin(dto: SigninDto): Promise<SigninResponseDto>;
    private createHasPassword;
}
