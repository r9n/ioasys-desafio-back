"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const common_1 = require("@nestjs/common");
const user_service_1 = require("../user/user.service");
const signin_response_dto_1 = require("./dto/signin-response.dto");
const user_entity_1 = require("../user/user.entity");
const bcrypt = require("bcrypt");
const exceptions_message_1 = require("../../utils/messages/exceptions.message");
const signup_response_dto_1 = require("./dto/signup-response.dto");
const role_service_1 = require("./roles/role.service");
const jwt_1 = require("@nestjs/jwt");
const forbidden_message_1 = require("../../utils/messages/forbidden.message");
let AuthService = class AuthService {
    constructor(userService, rolesService, jwtService) {
        this.userService = userService;
        this.rolesService = rolesService;
        this.jwtService = jwtService;
    }
    async signup(dto) {
        const newUser = new user_entity_1.User(dto);
        const hasAccount = await this.userService.findUserByEmail(dto.email);
        if (hasAccount) {
            throw new common_1.ForbiddenException(exceptions_message_1.objectAlreadyExistsMessage('e-mail'));
        }
        newUser.salt = await bcrypt.genSalt();
        newUser.role = await this.rolesService.findRoleByName('user');
        newUser.passwordHash = await this.createHasPassword(dto.password, newUser.salt);
        return this.userService
            .save(newUser)
            .then((user) => {
            return new signup_response_dto_1.SingupResponseDto({
                name: user.name,
                lastName: user.lastName,
                email: user.email,
                phoneNumber: user.phoneNumber,
                role: user.role,
            });
        })
            .catch((error) => {
            throw new common_1.InternalServerErrorException(error.message);
        });
    }
    async signin(dto) {
        const user = await this.userService.findUserByEmail(dto.email);
        if (!user) {
            throw new common_1.NotFoundException(exceptions_message_1.objectDoesNotExistsMessage('e-mail'));
        }
        if (!user.isActive) {
            throw new common_1.NotFoundException(exceptions_message_1.userAlreadyDesactivated);
        }
        const jwtPayload = { email: dto.email, role: user.role.role };
        const accessToken = this.jwtService.sign(jwtPayload);
        if (this.userService.validateUserPassword(dto)) {
            const signinReponseDto = new signin_response_dto_1.SigninResponseDto();
            signinReponseDto.tokenJwt = accessToken;
            signinReponseDto.userId = user.id;
            return signinReponseDto;
        }
        throw new common_1.ForbiddenException(forbidden_message_1.signinForbidden);
    }
    async createHasPassword(password, salt) {
        return bcrypt.hash(password, salt);
    }
};
AuthService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [user_service_1.UserService,
        role_service_1.RoleService,
        jwt_1.JwtService])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map