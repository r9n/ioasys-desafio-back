import { Role } from './role.entity';
import { RolesRepository } from './roles.repository';
export declare class RoleService {
    private rolesRepository;
    constructor(rolesRepository: RolesRepository);
    findRoleByName(role: string): Promise<Role>;
}
