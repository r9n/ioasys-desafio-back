"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Role = void 0;
const swagger_1 = require("@nestjs/swagger");
const constraints_1 = require("../../../config/constraints");
const user_entity_1 = require("../../user/user.entity");
const base_entity_1 = require("../../../utils/entities/base-entity");
const typeorm_1 = require("typeorm");
let Role = class Role extends base_entity_1.BaseEntity {
    constructor(partial) {
        super();
        Object.assign(this, partial);
    }
};
__decorate([
    typeorm_1.Column({ type: 'varchar', length: constraints_1.MAX_ROLE_NAME_LENGHT, nullable: false }),
    swagger_1.ApiProperty({
        type: String,
        maxLength: constraints_1.MAX_ROLE_NAME_LENGHT,
        minLength: constraints_1.MIN_ROLE_NAME_LENGHT,
        required: true,
    }),
    __metadata("design:type", String)
], Role.prototype, "role", void 0);
__decorate([
    typeorm_1.OneToMany(() => user_entity_1.User, (user) => user.role),
    __metadata("design:type", Array)
], Role.prototype, "user", void 0);
Role = __decorate([
    typeorm_1.Entity(),
    __metadata("design:paramtypes", [Object])
], Role);
exports.Role = Role;
//# sourceMappingURL=role.entity.js.map