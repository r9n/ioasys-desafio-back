import { User } from 'src/modules/user/user.entity';
import { BaseEntity } from 'src/utils/entities/base-entity';
export declare class Role extends BaseEntity {
    role: string;
    user: Array<User>;
    constructor(partial: Partial<Role>);
}
